﻿using AutoMapper;
using FishingStore.API.DTOs.CustomerDTOs.Requests;
using FishingStore.API.DTOs.CustomerDTOs.Responses;
using FishingStore.API.DTOs.OrderDTOs.Requests;
using FishingStore.API.DTOs.OrderDTOs.Responses;
using FishingStore.API.DTOs.ProductDTOs.Requests;
using FishingStore.API.DTOs.ProductDTOs.Responses;
using FishingStore.API.DTOs.PromotionDTOs.Requests;
using FishingStore.API.DTOs.PromotionDTOs.Responses;
using FishingStore.API.DTOs.ReviewDTOs.Requests;
using FishingStore.API.DTOs.ReviewDTOs.Responses;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Implementations;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;


namespace FishingStore.API.Mapping
{
    public class AutoMapping : Profile
    {
        public AutoMapping() {
            CreateMap<Customer, CustomerResponseDTO>();
            CreateMap<CreateCustomerRequestDTO, Customer>();
            CreateMap<EditCustomerRequestDTO, Customer>()
                .IncludeBase<CreateCustomerRequestDTO, Customer>();

            CreateMap<Order, OrderResponseDTO>();
            CreateMap<CreateOrderRequestDTO, Order>();
            CreateMap<EditOrderRequestDTO, Order>()
                .IncludeBase<CreateOrderRequestDTO, Order>();

            CreateMap<ProductWithCategory, ProductResponseDTO>();
            CreateMap<CreateProductRequestDTO, Product>();
            CreateMap<EditProductRequestDTO, Product>()
                .IncludeBase<CreateProductRequestDTO, Product>();

            CreateMap<Promotion, PromotionResponseDTO>();
            CreateMap<CreatePromotionRequestDTO, Promotion>();
            CreateMap<EditPromotionRequestDTO, Promotion>()
                .IncludeBase<CreatePromotionRequestDTO, Promotion>();

            CreateMap<Review, ReviewResponseDTO>();
            CreateMap<CreateReviewRequestDTO, Review>();
            CreateMap<EditReviewRequestDTO, Review>()
                .IncludeBase<CreateReviewRequestDTO, Review>();
        }
    }
}
