﻿namespace FishingStore.API.MethodResults
{
    public class MethodResultFactory
    {
        public ILogger<MethodResultFactory> _logger;

        public MethodResultFactory(ILogger<MethodResultFactory> logger)
        {
            _logger = logger;
        }

        public MethodResult<T> Create<T>()
            where T : class
        {
            return new MethodResult<T>(_logger);
        }
    }
}
