﻿using AutoMapper;
using FishingStore.API.Mapping;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Implementations;
using FishingStore.API.Services.Interfaces;
/*using FishingStore.API.Services.Implementations;
using FishingStore.API.Services.Interfaces;*/
using FishingStoreDB.Repositories.Implementations;
using FishingStoreDB.Repositories.Interfaces;

namespace FishingStore.API
{
    public static class ServicesExtensionMethods
    {
        public static void AddOriginCommunication(this IServiceCollection services)
        {
            // for Angular SPA
            services.AddCors(options => options.AddPolicy("UsersOrigins", policy =>
            {
                policy.WithOrigins("http://localhost:4200").AllowAnyHeader().AllowAnyMethod();
            }));
        }

        public static void AddMapper(this IServiceCollection services)
        {
            var mapperConfig = new MapperConfiguration(mc =>
            {
                mc.AllowNullCollections = true;

                mc.AddProfile(new AutoMapping());
            });

            var mapper = mapperConfig.CreateMapper();
            services.AddSingleton(mapper);
        }

        public static void AddRepositories(this IServiceCollection services)
        {
            services.AddScoped(typeof(IBaseRepository<>), typeof(BaseRepository<>));

            services.AddScoped<IUnitOfWork, UnitOfWork>();
            services.AddScoped<ICustomerRepository, CustomerRepository>();
            services.AddScoped<IOrderRepository, OrderRepository>();
            services.AddScoped<IProductRepository, ProductRepository>();
            services.AddScoped<IPromotionRepository, PromotionRepository>();
            services.AddScoped<IReviewRepository, ReviewRepository>();
        }

        public static void AddCustomServices(this IServiceCollection services)
        {
            services.AddScoped<MethodResultFactory>();
            services.AddScoped<ICustomerAddressService, CustomerAddressService>();
            services.AddScoped<ICustomerService, CustomerService>();
            services.AddScoped<IOrderDetailService, OrderDetailService>();
            services.AddScoped<IOrderService, OrderService>();
            services.AddScoped<IOrderTrackingService, OrderTrackingService>();
            services.AddScoped<IProductService, ProductService>();
            services.AddScoped<IPromotionService, PromotionService>();
            services.AddScoped<IReviewService, ReviewService>();
            services.AddScoped<IShoppingCartService, ShoppingCartService>();
        }
    }
}
