﻿using FishingStore.API.MethodResults;
using FishingStore.API.DTOs.CustomerDTOs.Requests;
using FishingStore.API.DTOs.CustomerDTOs.Responses;
using FishingStore.API.DTOs;

namespace FishingStore.API.Services.Interfaces
{
    public interface ICustomerService
    {
        MethodResult<List<CustomerResponseDTO>> GetCustomers();
        Task<MethodResult<CreatedEntityDTO>> CreateCustomerAsync(CreateCustomerRequestDTO createCustomerRequestDTO);
        Task<MethodResult<CreatedEntityDTO>> EditCustomerAsync(EditCustomerRequestDTO editCustomerRequestDTO);
        Task<MethodResult<string>> DeleteCustomerAsync(int busId);
    }
}
