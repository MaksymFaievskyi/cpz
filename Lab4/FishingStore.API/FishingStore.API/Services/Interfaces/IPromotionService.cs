﻿using FishingStore.API.DTOs;
using FishingStore.API.MethodResults;
using FishingStore.API.DTOs.PromotionDTOs.Requests;
using FishingStore.API.DTOs.PromotionDTOs.Responses;
using FishingStore.API.MethodResults;

namespace FishingStore.API.Services.Interfaces
{
    public interface IPromotionService
    {
        MethodResult<List<PromotionResponseDTO>> GetPromotions();
        Task<MethodResult<CreatedEntityDTO>> CreatePromotionAsync(CreatePromotionRequestDTO createPromotionRequestDTO);
        Task<MethodResult<CreatedEntityDTO>> EditPromotionAsync(EditPromotionRequestDTO editPromotionRequestDTO);
        Task<MethodResult<string>> DeletePromotionAsync(int promotionId);
    }
}
