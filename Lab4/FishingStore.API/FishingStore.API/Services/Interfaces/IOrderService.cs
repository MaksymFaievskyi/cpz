﻿using FishingStore.API.MethodResults;
using FishingStore.API.DTOs.OrderDTOs.Requests;
using FishingStore.API.DTOs.OrderDTOs.Responses;
using FishingStore.API.DTOs;

namespace FishingStore.API.Services.Interfaces
{
    public interface IOrderService
    {
        MethodResult<List<OrderResponseDTO>> GetOrders();
        Task<MethodResult<CreatedEntityDTO>> CreateOrderAsync(CreateOrderRequestDTO createOrderRequestDTO);
        Task<MethodResult<CreatedEntityDTO>> EditOrderAsync(EditOrderRequestDTO editOrderRequestDTO);
        Task<MethodResult<string>> DeleteOrderAsync(int orderId);
    }
}
