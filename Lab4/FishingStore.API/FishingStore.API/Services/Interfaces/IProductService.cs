﻿using FishingStore.API.DTOs;
using FishingStore.API.DTOs.ProductDTOs.Requests;
using FishingStore.API.DTOs.ProductDTOs.Responses;
using FishingStore.API.MethodResults;

namespace FishingStore.API.Services.Interfaces
{
    public interface IProductService
    {
        MethodResult<List<ProductResponseDTO>> GetProducts();
        Task<MethodResult<CreatedEntityDTO>> CreateProductAsync(CreateProductRequestDTO createProductRequestDTO);
        Task<MethodResult<CreatedEntityDTO>> EditProductAsync(EditProductRequestDTO editProductRequestDTO);
        Task<MethodResult<string>> DeleteProductAsync(int productId);
    }
}
