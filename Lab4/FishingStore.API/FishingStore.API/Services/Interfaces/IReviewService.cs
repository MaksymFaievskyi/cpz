﻿using FishingStore.API.DTOs;
using FishingStore.API.MethodResults;
using FishingStore.API.DTOs.ReviewDTOs.Requests;
using FishingStore.API.DTOs.ReviewDTOs.Responses;
using FishingStore.API.MethodResults;

namespace FishingStore.API.Services.Interfaces
{
    public interface IReviewService
    {
        MethodResult<List<ReviewResponseDTO>> GetReviews();
        Task<MethodResult<CreatedEntityDTO>> CreateReviewAsync(CreateReviewRequestDTO createPassengerRequestDTO);
        Task<MethodResult<CreatedEntityDTO>> EditReviewAsync(EditReviewRequestDTO editPassengerRequestDTO);
        Task<MethodResult<string>> DeleteReviewAsync(int reviewId);
    }
}
