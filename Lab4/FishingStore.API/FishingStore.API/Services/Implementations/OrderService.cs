﻿using AutoMapper;
using FishingStore.API.DTOs;
using FishingStore.API.DTOs.OrderDTOs.Requests;
using FishingStore.API.DTOs.OrderDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;

namespace FishingStore.API.Services.Implementations
{
    public class OrderService : IOrderService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly MethodResultFactory _methodResultFactory;

        public OrderService(
           IUnitOfWork unitOfWork,
           IMapper mapper,
           MethodResultFactory methodResultFactory)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _methodResultFactory = methodResultFactory;
        }

        public MethodResult<List<OrderResponseDTO>> GetOrders()
        {
            var result = _methodResultFactory.Create<List<OrderResponseDTO>>();

            var orders = _unitOfWork.OrderRepository.GetAll();

            result.Data = _mapper.Map<List<OrderResponseDTO>>(orders);

            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> CreateOrderAsync(CreateOrderRequestDTO createOrderRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();
            if (result.IsError)
            {
                return result;
            }

            var order = _mapper.Map<Order>(createOrderRequestDTO);
            _unitOfWork.OrderRepository.Insert(order);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(order.OrderId);
            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> EditOrderAsync(EditOrderRequestDTO editOrderRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            var order = _unitOfWork.OrderRepository.GetById(editOrderRequestDTO.OrderId);
            if (order == null)
            {
                result.SetError($"Order with ID {editOrderRequestDTO.OrderId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _mapper.Map(editOrderRequestDTO, order);
            _unitOfWork.OrderRepository.Update(order);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(order.OrderId);

            return result;
        }

        public async Task<MethodResult<string>> DeleteOrderAsync(int orderId)
        {
            var result = _methodResultFactory.Create<string>();

            var order = _unitOfWork.OrderRepository.GetById(orderId);
            if (order == null)
            {
                result.SetError($"Order with ID {orderId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _unitOfWork.OrderRepository.Delete(order);
            await _unitOfWork.CommitAsync();

            result.Data = $"Order with ID {orderId} was deleted";
            return result;
        }
    }
}
