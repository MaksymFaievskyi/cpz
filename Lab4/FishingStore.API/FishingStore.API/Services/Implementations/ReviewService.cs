﻿using AutoMapper;
using FishingStore.API.DTOs;
using FishingStore.API.DTOs.ReviewDTOs.Requests;
using FishingStore.API.DTOs.ReviewDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;

namespace FishingStore.API.Services.Implementations
{
    public class ReviewService : IReviewService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly MethodResultFactory _methodResultFactory;

        public ReviewService(
           IUnitOfWork unitOfWork,
           IMapper mapper,
           MethodResultFactory methodResultFactory)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _methodResultFactory = methodResultFactory;
        }

        public MethodResult<List<ReviewResponseDTO>> GetReviews()
        {
            var result = _methodResultFactory.Create<List<ReviewResponseDTO>>();

            var reviews = _unitOfWork.ReviewRepository.GetAll();

            result.Data = _mapper.Map<List<ReviewResponseDTO>>(reviews);

            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> CreateReviewAsync(CreateReviewRequestDTO createReviewRequestDTO)
        {
            var result = ValidateRequest(createReviewRequestDTO);
            if (result.IsError)
            {
                return result;
            }

            var review = _mapper.Map<Review>(createReviewRequestDTO);
            _unitOfWork.ReviewRepository.Insert(review);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(review.ReviewId);
            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> EditReviewAsync(EditReviewRequestDTO editReviewRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            var review = _unitOfWork.ReviewRepository.GetById(editReviewRequestDTO.ReviewId);
            if (review == null)
            {
                result.SetError($"Review with ID {editReviewRequestDTO.ReviewId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            result = ValidateRequest(editReviewRequestDTO);

            _mapper.Map(editReviewRequestDTO, review);
            _unitOfWork.ReviewRepository.Update(review);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(review.ReviewId);

            return result;
        }

        public async Task<MethodResult<string>> DeleteReviewAsync(int reviewId)
        {
            var result = _methodResultFactory.Create<string>();

            var review = _unitOfWork.ReviewRepository.GetById(reviewId);
            if (review == null)
            {
                result.SetError($"Review with ID {reviewId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _unitOfWork.ReviewRepository.Delete(review);
            await _unitOfWork.CommitAsync();

            result.Data = $"Review with ID {reviewId} was deleted";
            return result;
        }

        private MethodResult<CreatedEntityDTO> ValidateRequest(CreateReviewRequestDTO createReviewRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();
            if (createReviewRequestDTO.ProductId != null)
            {
                var product = _unitOfWork.ProductRepository.GetById(createReviewRequestDTO.ProductId.Value);
                if (product == null)
                {
                    result.SetError($"Product with ID {createReviewRequestDTO.ProductId} not found", System.Net.HttpStatusCode.BadRequest);
                }
            }

            if (createReviewRequestDTO.CustomerId != null)
            {
                var customer = _unitOfWork.CustomerRepository.GetById(createReviewRequestDTO.CustomerId.Value);
                if (customer == null)
                {
                    result.SetError($"Customer with ID {createReviewRequestDTO.CustomerId} not found", System.Net.HttpStatusCode.BadRequest);
                }
            }

            return result;
        }

    }
}
