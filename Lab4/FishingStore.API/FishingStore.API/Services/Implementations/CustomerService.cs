﻿using AutoMapper;
using FishingStore.API.DTOs;
using FishingStore.API.DTOs.CustomerDTOs.Requests;
using FishingStore.API.DTOs.CustomerDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;


namespace FishingStore.API.Services.Implementations
{
    public class CustomerService : ICustomerService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly MethodResultFactory _methodResultFactory;

        public CustomerService(
            IUnitOfWork unitOfWork,
            IMapper mapper,
            MethodResultFactory methodResultFactory)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _methodResultFactory = methodResultFactory;
        }

        public MethodResult<List<CustomerResponseDTO>> GetCustomers()
        {
            var result = _methodResultFactory.Create<List<CustomerResponseDTO>>();

            var customers = _unitOfWork.CustomerRepository.GetAll();

            result.Data = _mapper.Map<List<CustomerResponseDTO>>(customers);

            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> CreateCustomerAsync(CreateCustomerRequestDTO createCustomerRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            var customer = _mapper.Map<Customer>(createCustomerRequestDTO);

            _unitOfWork.CustomerRepository.Insert(customer);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(customer.CustomerId);
            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> EditCustomerAsync(EditCustomerRequestDTO editCustomerRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            var customer = _unitOfWork.CustomerRepository.GetById(editCustomerRequestDTO.CustomerId);
            if (customer == null)
            {
                result.SetError($"Customer with ID {editCustomerRequestDTO.CustomerId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _mapper.Map(editCustomerRequestDTO, customer);
            _unitOfWork.CustomerRepository.Update(customer);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(customer.CustomerId);

            return result;
        }

        public async Task<MethodResult<string>> DeleteCustomerAsync(int customerId)
        {
            var result = _methodResultFactory.Create<string>();

            var bus = _unitOfWork.CustomerRepository.GetById(customerId);
            if (bus == null)
            {
                result.SetError($"Customer with ID {customerId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _unitOfWork.CustomerRepository.Delete(bus);
            await _unitOfWork.CommitAsync();

            result.Data = $"Customer with ID {customerId} was deleted";

            return result;
        }


    }
}
