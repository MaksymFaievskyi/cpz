﻿using AutoMapper;
using FishingStore.API.DTOs;
using FishingStore.API.DTOs.ProductDTOs.Requests;
using FishingStore.API.DTOs.ProductDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Implementations;
using FishingStoreDB.Repositories.Interfaces;


namespace FishingStore.API.Services.Implementations
{
    public class ProductService : IProductService
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly MethodResultFactory _methodResultFactory;
        public ProductService(
           IUnitOfWork unitOfWork,
           IMapper mapper,
           MethodResultFactory methodResultFactory)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _methodResultFactory = methodResultFactory;
        }

        public MethodResult<List<ProductResponseDTO>> GetProducts()
        {
            var result = _methodResultFactory.Create<List<ProductResponseDTO>>();

            //var products = _unitOfWork.ProductRepository.GetAll();

            var products = _unitOfWork.ProductRepository.GetProductWithCategory();

            result.Data = _mapper.Map<List<ProductResponseDTO>>(products);

            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> CreateProductAsync(CreateProductRequestDTO createProductRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            if (createProductRequestDTO.CategoryId != null)
            {
                var productCategory = _unitOfWork.ProductCategoryRepository.GetById(createProductRequestDTO.CategoryId);
                if (productCategory == null)
                {
                    result.SetError($"Product category with ID {createProductRequestDTO.CategoryId} not found", System.Net.HttpStatusCode.BadRequest);
                    return result;
                }
            }

            var product = _mapper.Map<Product>(createProductRequestDTO);

            /*var newProduct = new Product
            {
                CategoryId = product.CategoryID,
                ProductName = product.ProductName,
                Description = product.Description,
                Price = product.Price,
                StockQuantity = product.StockQuantity
            };*/

            _unitOfWork.ProductRepository.Insert(product);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(product.ProductId);
            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> EditProductAsync(EditProductRequestDTO editProductRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            var product = _unitOfWork.ProductRepository.GetById(editProductRequestDTO.ProductId);
            if (product == null)
            {
                result.SetError($"Product with ID {editProductRequestDTO.ProductId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            if (editProductRequestDTO.CategoryId != null)
            {
                var productCategory = _unitOfWork.ProductCategoryRepository.GetById(editProductRequestDTO.CategoryId);
                if (productCategory == null)
                {
                    result.SetError($"Product category with ID {editProductRequestDTO.CategoryId} not found", System.Net.HttpStatusCode.BadRequest);
                    return result;
                }
            }

            _mapper.Map(editProductRequestDTO, product);
            _unitOfWork.ProductRepository.Update(product);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(product.ProductId);

            return result;
        }

        public async Task<MethodResult<string>> DeleteProductAsync(int productId)
        {
            var result = _methodResultFactory.Create<string>();

            var product = _unitOfWork.ProductRepository.GetById(productId);
            if (product == null)
            {
                result.SetError($"Product with ID {productId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _unitOfWork.ProductRepository.Delete(product);
            await _unitOfWork.CommitAsync();

            result.Data = $"Product with ID {productId} was deleted";

            return result;
        }

    }
}
