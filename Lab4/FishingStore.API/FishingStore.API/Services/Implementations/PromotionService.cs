﻿using AutoMapper;
using FishingStore.API.DTOs;
using FishingStore.API.DTOs.PromotionDTOs.Requests;
using FishingStore.API.DTOs.PromotionDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;
namespace FishingStore.API.Services.Implementations
{
    public class PromotionService : IPromotionService
    {

        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;
        private readonly MethodResultFactory _methodResultFactory;

        public PromotionService(
           IUnitOfWork unitOfWork,
           IMapper mapper,
           MethodResultFactory methodResultFactory)
        {
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _methodResultFactory = methodResultFactory;
        }

        public MethodResult<List<PromotionResponseDTO>> GetPromotions()
        {
            var result = _methodResultFactory.Create<List<PromotionResponseDTO>>();

            var promotions = _unitOfWork.PromotionRepository.GetAll();

            result.Data = _mapper.Map<List<PromotionResponseDTO>>(promotions);

            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> CreatePromotionAsync(CreatePromotionRequestDTO createPromotionRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();
            if (result.IsError)
            {
                return result;
            }

            var promotion = _mapper.Map<Promotion>(createPromotionRequestDTO);
            _unitOfWork.PromotionRepository.Insert(promotion);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(promotion.PromotionId);
            return result;
        }

        public async Task<MethodResult<CreatedEntityDTO>> EditPromotionAsync(EditPromotionRequestDTO editPromotionRequestDTO)
        {
            var result = _methodResultFactory.Create<CreatedEntityDTO>();

            var promotion = _unitOfWork.PromotionRepository.GetById(editPromotionRequestDTO.PromotionId);
            if (promotion == null)
            {
                result.SetError($"Promotion with ID {editPromotionRequestDTO.PromotionId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _mapper.Map(editPromotionRequestDTO, promotion);
            _unitOfWork.PromotionRepository.Update(promotion);
            await _unitOfWork.CommitAsync();

            result.Data = new CreatedEntityDTO(promotion.PromotionId);

            return result;
        }

        public async Task<MethodResult<string>> DeletePromotionAsync(int promotionId)
        {
            var result = _methodResultFactory.Create<string>();

            var order = _unitOfWork.PromotionRepository.GetById(promotionId);
            if (order == null)
            {
                result.SetError($"Promotion with ID {promotionId} not found", System.Net.HttpStatusCode.NotFound);
                return result;
            }

            _unitOfWork.PromotionRepository.Delete(order);
            await _unitOfWork.CommitAsync();

            result.Data = $"Promotion with ID {promotionId} was deleted";
            return result;
        }
    }
}
