﻿using FishingStore.API.Utils;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace FishingStore.API.CustomAttributes
{
    public class CustomEmailAttribute : ValidationAttribute
    {
        public CustomEmailAttribute()
           : base("Please enter valid email address")
        { 
        }

        public override bool IsValid(object value)
        {
            try
            {
                if(value.ToString() == "")
                    return true;
                else
                return value == null || Regex.IsMatch(
                    value.ToString(), 
                    RegexConstants.Email,
                    RegexOptions.None,
                    TimeSpan.FromSeconds(1));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
