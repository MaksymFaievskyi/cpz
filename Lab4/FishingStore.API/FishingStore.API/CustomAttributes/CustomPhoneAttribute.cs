﻿using FishingStore.API.Utils;
using Microsoft.VisualBasic;
using System.ComponentModel.DataAnnotations;
using System.Text.RegularExpressions;

namespace FishingStore.API.CustomAttributes
{
    public class CustomPhoneAttribute : ValidationAttribute
    {
        public CustomPhoneAttribute()
           : base("Please enter valid phone number")
        {
        }

        public override bool IsValid(object value)
        {
            try
            {
                return value == null || Regex.IsMatch(value.ToString(), RegexConstants.PhoneNumberPattern, RegexOptions.None, TimeSpan.FromSeconds(1));
            }
            catch (RegexMatchTimeoutException)
            {
                return false;
            }
        }
    }
}
