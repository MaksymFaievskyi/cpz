﻿using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.CustomAttributes
{
    public class RequiredGreaterThanZeroAttribute : ValidationAttribute
    {
        private readonly int _minInt = 0;
        private readonly decimal _minDecimal = 0m;
        private readonly double _minDouble = 0;

        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            return IsValidType(value) ? ValidationResult.Success : new ValidationResult($"The field {validationContext.DisplayName} must be greater than 0");
        }

        protected virtual bool IsValidType(object value)
        {
            if (int.TryParse(value.ToString(), out var intValue))
            {
                return intValue > _minInt;
            }
            else if (decimal.TryParse(value.ToString(), out var decimalValue))
            {
                return decimalValue > _minDecimal;
            }
            else if (double.TryParse(value.ToString(), out var doubleValue))
            {
                return doubleValue > _minDouble;
            }

            return false;
        }
    }
}
