﻿using FishingStore.API.DTOs;
using FishingStore.API.DTOs.ReviewDTOs.Requests;
using FishingStore.API.DTOs.ReviewDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Implementations;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.Controllers
{
    public class ReviewController : BaseApiController
    {
        private readonly IReviewService _reviewService;
        public ReviewController(IReviewService reviewService)
        {
            _reviewService = reviewService;
        }

        [HttpGet("GetAllReviews")]
        public ActionResult<MethodResult<List<ReviewResponseDTO>>> GetAllReviews()
        {
            var result = _reviewService.GetReviews();

            return result.DecideWhatToReturn();
        }

        [HttpPost("CreateReview")]
        public async Task<ActionResult<CreatedEntityDTO>> CreateReview([FromBody] CreateReviewRequestDTO createReviewRequestDTO)
        {
            var result = await _reviewService.CreateReviewAsync(createReviewRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpPut("EditReview")]
        public async Task<ActionResult<CreatedEntityDTO>> EditReview([FromBody] EditReviewRequestDTO editReviewRequestDTO)
        {
            var result = await _reviewService.EditReviewAsync(editReviewRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpDelete("DeleteReview/{reviewId}")]
        public async Task<ActionResult<string>> DeleteReview([Required] int reviewId)
        {
            var result = await _reviewService.DeleteReviewAsync(reviewId);

            return result.DecideWhatToReturn();
        }
    }
}
