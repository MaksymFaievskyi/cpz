﻿using FishingStore.API.DTOs;
using FishingStore.API.DTOs.CustomerDTOs.Requests;
using FishingStore.API.DTOs.CustomerDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.Controllers
{
    public class CustomerController : BaseApiController
    {
        private readonly ICustomerService _customerService;
        public CustomerController(ICustomerService customerService)
        {
            _customerService = customerService;
        }

        /// <summary>
        /// Retrive list of customers
        /// </summary>
        /// <returns>List of customer info</returns>
        /// <response code="200"></response>
        [HttpGet("GetAllCustomers")]
        [ProducesResponseType(typeof(CustomerResponseDTO), StatusCodes.Status200OK)]
        public ActionResult<List<CustomerResponseDTO>> GetAllCustomers()
        {
            var result = _customerService.GetCustomers();

            return result.DecideWhatToReturn();
        }

        /// <summary>
        /// Create a customer
        /// </summary>
        /// <param name="CreateCustomerRequestDTO">New customer info</param>
        /// <returns>Id of created customer</returns>
        [HttpPost("CreateCustomer")]
        public async Task<ActionResult<CreatedEntityDTO>> CreateCustomer([FromBody] CreateCustomerRequestDTO createCustomerRequestDTO)
        {
            var result = await _customerService.CreateCustomerAsync(createCustomerRequestDTO);

            return result.DecideWhatToReturn();
        }

        /// <summary>
        /// Edit specified customer
        /// </summary>
        /// <param name="editCustomerRequestDTO">Customer new data to update</param>
        /// <returns>Updated info about customer</returns>
        /// <response code="200">Returns the newly edited item</response>
        /// <response code="404">If customer with specified ID does not exists</response>
        [HttpPut("EditCustomer")]
        [ProducesResponseType(typeof(CreatedEntityDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(typeof(ErrorResponse), StatusCodes.Status404NotFound)]
        public async Task<ActionResult<CreatedEntityDTO>> EditCustomer([FromBody] EditCustomerRequestDTO editCustomerRequestDTO)
        {
            var result = await _customerService.EditCustomerAsync(editCustomerRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpDelete("DeleteCustomer/{customerId}")]
        [ProducesResponseType(typeof(CreatedEntityDTO), StatusCodes.Status200OK)]
        [ProducesResponseType(StatusCodes.Status404NotFound)]
        public async Task<ActionResult<string>> DeleteCustomer([Required] int customerId)
        {
            var result = await _customerService.DeleteCustomerAsync(customerId);

            return result.DecideWhatToReturn();
        }
    }
}
