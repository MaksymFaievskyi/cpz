﻿using FishingStore.API.DTOs;
using FishingStore.API.DTOs.PromotionDTOs.Requests;
using FishingStore.API.DTOs.PromotionDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.Controllers
{
    public class PromotionController : BaseApiController
    {

        private readonly IPromotionService _promotionService;
        public PromotionController(IPromotionService promotionService)
        {
            _promotionService = promotionService;
        }

        [HttpGet("GetAllPromotions")]
        public ActionResult<MethodResult<List<PromotionResponseDTO>>> GetAllPromotions()
        {
            var result = _promotionService.GetPromotions();

            return result.DecideWhatToReturn();
        }

        [HttpPost("CreatePromotion")]
        public async Task<ActionResult<CreatedEntityDTO>> CreatePromotion([FromBody] CreatePromotionRequestDTO createPromotionRequestDTO)
        {
            var result = await _promotionService.CreatePromotionAsync(createPromotionRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpPut("EditDriver")]
        public async Task<ActionResult<CreatedEntityDTO>> EditPromotion([FromBody] EditPromotionRequestDTO editPromotionRequestDTO)
        {
            var result = await _promotionService.EditPromotionAsync(editPromotionRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpDelete("DeletePromotion/{promotionId}")]
        public async Task<ActionResult<string>> DeletePromotion([Required] int promotionId)
        {
            var result = await _promotionService.DeletePromotionAsync(promotionId);

            return result.DecideWhatToReturn();
        }
    }
}
