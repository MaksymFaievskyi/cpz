﻿using FishingStore.API.DTOs;
using FishingStore.API.DTOs.ProductDTOs.Requests;
using FishingStore.API.DTOs.ProductDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.Controllers
{
    public class ProductController : BaseApiController
    {
        private readonly IProductService _productService;
        public ProductController(IProductService productService)
        {
            _productService = productService;
        }

        /// <summary>
        /// Retrive list of products
        /// </summary>
        /// <returns>List of product info</returns>
        /// <response code="200"></response>
        [HttpGet("GetAllProducts")]
        public ActionResult<MethodResult<List<ProductResponseDTO>>> GetAllProducts()
        {
            var result = _productService.GetProducts();

            return result.DecideWhatToReturn();
        }

        /// <summary>
        /// Create a product
        /// </summary>
        /// <param name="createProductRequestDTO">New product info</param>
        /// <returns>Id of created product</returns>
        [HttpPost("CreateProduct")]
        public async Task<ActionResult<CreatedEntityDTO>> CreateProduct([FromBody] CreateProductRequestDTO createProductRequestDTO)
        {
            var result = await _productService.CreateProductAsync(createProductRequestDTO);

            return result.DecideWhatToReturn();
        }

        /// <summary>
        /// Edit specified product
        /// </summary>
        /// <param name="editProductRequestDTO">Product new data to update</param>
        /// <returns>Updated info about product</returns>
        /// <response code="200">Returns the newly edited item</response>
        /// <response code="404">If product with specified ID does not exists</response>
        [HttpPut("EditProduct")]
        public async Task<ActionResult<CreatedEntityDTO>> EditProduct([FromBody] EditProductRequestDTO editProductRequestDTO)
        {
            var result = await _productService.EditProductAsync(editProductRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpDelete("DeleteProduct/{productId}")]
        public async Task<ActionResult<string>> DeleteProduct([Required] int productId)
        {
            var result = await _productService.DeleteProductAsync(productId);

            return result.DecideWhatToReturn();
        }
    }
}
