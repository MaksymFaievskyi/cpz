﻿using Microsoft.AspNetCore.Mvc;

namespace FishingStore.API.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    public class BaseApiController : ControllerBase
    {
    }
}
