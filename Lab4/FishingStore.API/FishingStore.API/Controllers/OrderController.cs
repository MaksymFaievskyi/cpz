﻿using FishingStore.API.DTOs;
using FishingStore.API.DTOs.OrderDTOs.Requests;
using FishingStore.API.DTOs.OrderDTOs.Responses;
using FishingStore.API.MethodResults;
using FishingStore.API.Services.Interfaces;
using FishingStoreDB.Models;
using Microsoft.AspNetCore.Mvc;
using System.ComponentModel.DataAnnotations;


namespace FishingStore.API.Controllers
{
    public class OrderController : BaseApiController
    {
        private readonly IOrderService _orderService;
        public OrderController(IOrderService orderService)
        {
            _orderService = orderService;
        }

        [HttpGet("GetAllOrders")]
        public ActionResult<MethodResult<List<OrderResponseDTO>>> GetAllOrders()
        {
            var result = _orderService.GetOrders();

            return result.DecideWhatToReturn();
        }

        [HttpPost("CreateOrder")]
        public async Task<ActionResult<CreatedEntityDTO>> CreateOrder([FromBody] CreateOrderRequestDTO createOrderRequestDTO)
        {
            var result = await _orderService.CreateOrderAsync(createOrderRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpPut("EditOrder")]
        public async Task<ActionResult<CreatedEntityDTO>> EditOrder([FromBody] EditOrderRequestDTO editOrderRequestDTO)
        {
            var result = await _orderService.EditOrderAsync(editOrderRequestDTO);

            return result.DecideWhatToReturn();
        }

        [HttpDelete("DeleteOrder/{orderId}")]
        public async Task<ActionResult<string>> DeleteOrder([Required] int orderId)
        {
            var result = await _orderService.DeleteOrderAsync(orderId);

            return result.DecideWhatToReturn();
        }
    }
}
