﻿namespace FishingStore.API.Utils
{
    public static class Constants
    {
        public const string NameErrorMessage = "Name/surname can't be less than 3 or longer than 20 chars";
        public const string CompanyNameErrorMessage = "Company name can't be less than 3 or longer than 20 chars";
        public const string PhoneErrorMessage = "Phone number can't be less than 8 chars or longer than 20";
    }
}
