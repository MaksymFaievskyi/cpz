﻿namespace FishingStore.API.Utils
{
    public static class RegexConstants
    {
        public const string Email = @"^[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$";
        public const string PhoneNumberPattern = @"^[+]*[(]{0,1}[0-9]{1,4}[)]{0,1}[-\s\./0-9]*$";
    }
}
