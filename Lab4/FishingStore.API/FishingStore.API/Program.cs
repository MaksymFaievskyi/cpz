using FishingStore.API;
using FishingStore.API.Middlewares;
using FishingStoreDB.Data;
using Microsoft.EntityFrameworkCore;
using System.Reflection;



var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen(
    c =>
    {
        c.SwaggerDoc("v1", new Microsoft.OpenApi.Models.OpenApiInfo
        {
            Title = "Fishing Store API",
            Description = "An ASP.NET Core Web API for managing fishing store",
            Version = "v1"
        });

        var xmlFilename = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
        c.IncludeXmlComments(Path.Combine(AppContext.BaseDirectory, xmlFilename));
    }
);
builder.Services.AddDbContext<FishingStoreDbContext>(options =>
  options.UseSqlServer(builder.Configuration.GetConnectionString("Default"))
);


builder.Services.AddRepositories();
builder.Services.AddMapper();
builder.Services.AddCustomServices();
builder.Services.AddOriginCommunication();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseMiddleware<ExceptionMiddleware>();

app.UseHttpsRedirection();

app.UseRouting();
app.UseCors("UsersOrigins");
app.UseAuthorization();

app.MapControllers();

app.Run();
