﻿using FishingStore.API.CustomAttributes;

namespace FishingStore.API.DTOs.ReviewDTOs.Requests
{
    public class EditReviewRequestDTO : CreateReviewRequestDTO
    {
        [RequiredGreaterThanZero]
        public int ReviewId { get; set; }
    }
}
