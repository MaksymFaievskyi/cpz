﻿using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.ReviewDTOs.Requests
{
    public class CreateReviewRequestDTO
    {
        public int? ProductId { get; set; }

        public int? CustomerId { get; set; }

        [Required]
        /*[MinLength(0)]
        [MaxLength(5)]*/
        public int? Rating { get; set; }

        [StringLength(100, MinimumLength = 0)]
        public string? Comment { get; set; }

        public DateTime? ReviewDate { get; set; }
    }
}
