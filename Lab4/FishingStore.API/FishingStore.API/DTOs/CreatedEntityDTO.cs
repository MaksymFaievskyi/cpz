﻿namespace FishingStore.API.DTOs
{
    public class CreatedEntityDTO
    {
        public int Id { get; set; }

        public CreatedEntityDTO(int id)
        {
            Id = id;
        }
    }
}
