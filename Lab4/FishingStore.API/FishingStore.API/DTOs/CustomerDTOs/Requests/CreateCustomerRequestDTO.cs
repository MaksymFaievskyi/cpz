﻿using FishingStore.API.CustomAttributes;
using FishingStore.API.Utils;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.CustomerDTOs.Requests
{
    public class CreateCustomerRequestDTO
    {
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = Constants.NameErrorMessage)]
        public string? FirstName { get; set; }
        [Required]
        [StringLength(20, MinimumLength = 3, ErrorMessage = Constants.NameErrorMessage)]
        public string? LastName { get; set; }
        [Required]
        [MinLength(1)]
        [MaxLength(1)]
        public string? Gender { get; set; }
        [Required]
        [Range(1, 100)]
        public int? Age { get; set; }
        [CustomEmailAttribute]
        public string? Email { get; set; }
        [CustomPhoneAttribute]
        [Required]
        public string? Phone { get; set; }
    }
}
