﻿using FishingStore.API.CustomAttributes;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.CustomerDTOs.Requests
{
    public class EditCustomerRequestDTO : CreateCustomerRequestDTO
    {
        [RequiredGreaterThanZero]
        public int CustomerId { get; set; }
    }
}
