﻿namespace FishingStore.API.DTOs.ProductDTOs.Responses
{
    public class ProductResponseDTO
    {
        public int ProductId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryId { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal DiscountedPrice { get; set; }
        public decimal BasePrice { get; set; }
        public int StockQuantity { get; set; }
    }
}
