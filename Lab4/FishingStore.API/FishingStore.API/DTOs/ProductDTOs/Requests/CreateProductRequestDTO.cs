﻿using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.ProductDTOs.Requests
{
    public class CreateProductRequestDTO
    {
        [Required]
        public int CategoryId { get; set; }
        [Required]
        public string ProductName { get; set; }
        [MinLength(0)]
        [MaxLength(1000)]
        public string Description { get; set; }
        [Required]
        [Range(0, 100000)]
        public decimal BasePrice { get; set; }
        [Range(0, 1000)]
        public int StockQuantity { get; set; }
    }
}
