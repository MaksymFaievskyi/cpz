﻿using FishingStore.API.CustomAttributes;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.ProductDTOs.Requests
{
    public class EditProductRequestDTO : CreateProductRequestDTO
    {
        [RequiredGreaterThanZero]
        public int ProductId { get; set; }
    }
}
