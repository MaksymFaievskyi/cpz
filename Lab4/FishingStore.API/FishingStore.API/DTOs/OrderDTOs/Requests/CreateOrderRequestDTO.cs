﻿using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.OrderDTOs.Requests
{
    public class CreateOrderRequestDTO
    {

        public int? CustomerId { get; set; }

        public DateTime? OrderDate { get; set; }
        [Required]
        [Range(0, 1000000)]
        public decimal? TotalAmount { get; set; }
    }
}
