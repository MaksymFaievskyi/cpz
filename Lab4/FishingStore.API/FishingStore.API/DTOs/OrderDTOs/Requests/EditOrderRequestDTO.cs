﻿using FishingStore.API.CustomAttributes;
using System.ComponentModel.DataAnnotations;

namespace FishingStore.API.DTOs.OrderDTOs.Requests
{
    public class EditOrderRequestDTO : CreateOrderRequestDTO
    {
        [RequiredGreaterThanZero]
        public int OrderId { get; set;}
    }
}
