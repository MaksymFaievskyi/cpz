﻿namespace FishingStore.API.DTOs.OrderDTOs.Responses
{
    public class OrderResponseDTO
    {
        public int OrderId { get; set; }

        public int? CustomerId { get; set; }

        public DateTime? OrderDate { get; set; }

        public decimal? TotalAmount { get; set; }
    }
}
