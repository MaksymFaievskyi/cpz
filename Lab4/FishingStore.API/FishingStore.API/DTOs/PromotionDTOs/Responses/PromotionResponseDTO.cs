﻿namespace FishingStore.API.DTOs.PromotionDTOs.Responses
{
    public class PromotionResponseDTO
    {
        public int PromotionId { get; set; }

        public int? ProductId { get; set; }

        public decimal? Discount { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
