﻿using FishingStore.API.CustomAttributes;

namespace FishingStore.API.DTOs.PromotionDTOs.Requests
{
    public class EditPromotionRequestDTO : CreatePromotionRequestDTO
    {
        [RequiredGreaterThanZero]
        public int PromotionId { get; set; }
    }
}
