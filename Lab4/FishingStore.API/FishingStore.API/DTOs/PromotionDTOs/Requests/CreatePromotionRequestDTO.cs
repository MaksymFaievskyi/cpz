﻿namespace FishingStore.API.DTOs.PromotionDTOs.Requests
{
    public class CreatePromotionRequestDTO
    {

        public int? ProductId { get; set; }

        public decimal? Discount { get; set; }

        public DateTime? StartDate { get; set; }

        public DateTime? EndDate { get; set; }
    }
}
