﻿using FishingStoreDB.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishingStoreDB.Repositories.Interfaces
{
    public interface IPromotionRepository : IBaseRepository <Promotion>
    {

    }
}
