﻿using FishingStoreDB.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishingStoreDB.Repositories.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        public IEnumerable<Order> GetOrderTracking();

        public IEnumerable<Order> GetOrderDetails();
    }
}
