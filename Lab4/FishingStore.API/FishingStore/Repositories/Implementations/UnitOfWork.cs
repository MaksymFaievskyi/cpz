﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FishingStoreDB.Data;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;

namespace FishingStoreDB.Repositories.Implementations
{
    public class UnitOfWork : IUnitOfWork
    {

        private readonly FishingStoreDbContext _context;

        private ICustomerRepository _customerRepository;
        public ICustomerRepository CustomerRepository => _customerRepository ??= new CustomerRepository(_context);

        private IOrderRepository _orderRepository;
        public IOrderRepository OrderRepository => _orderRepository ??= new OrderRepository(_context);

        private IProductRepository _productRepository;
        public IProductRepository ProductRepository => _productRepository ??= new ProductRepository(_context);

        private IPromotionRepository _promotionRepository;
        public IPromotionRepository PromotionRepository => _promotionRepository ??= new PromotionRepository(_context);

        private IReviewRepository _reviewRepository;
        public IReviewRepository ReviewRepository => _reviewRepository ??= new ReviewRepository(_context);

        private IBaseRepository<CustomerAddress> _customerAddressRepository;
        public IBaseRepository<CustomerAddress> CustomerAddressRepository => _customerAddressRepository ??= new BaseRepository<CustomerAddress>(_context);

        private IBaseRepository<OrderDetail> _orderDelailsRepository;
        public IBaseRepository<OrderDetail> OrderDetailRepository => _orderDelailsRepository ??= new BaseRepository<OrderDetail>(_context);

        private IBaseRepository<OrderTracking> _orderTrackingRepository;
        public IBaseRepository<OrderTracking> OrderTrackingRepository => _orderTrackingRepository ??= new BaseRepository<OrderTracking>(_context);

        private IBaseRepository<ProductCategory> _productCategoryRepository;
        public IBaseRepository<ProductCategory> ProductCategoryRepository => _productCategoryRepository ??= new BaseRepository<ProductCategory>(_context);

        private IBaseRepository<ShoppingCart> _shoppingCartRepository;
        public IBaseRepository<ShoppingCart> ShoppingCartRepository => _shoppingCartRepository ??= new BaseRepository<ShoppingCart>(_context);

        public void Commit()
        {
            _context.SaveChanges();
        }

        public async Task CommitAsync()
        {
            await _context.SaveChangesAsync();
        }

        public UnitOfWork(FishingStoreDbContext context)
        {
            _context = context;
        }
    }
}
