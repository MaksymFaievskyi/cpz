﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FishingStoreDB.Data;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;

namespace FishingStoreDB.Repositories.Implementations
{
    public class PromotionRepository : BaseRepository<Promotion>, IPromotionRepository
    {
        public PromotionRepository(FishingStoreDbContext context) : base(context) { }


    }
}
