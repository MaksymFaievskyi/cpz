﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FishingStoreDB.Repositories.Implementations
{
    public class ProductWithCategory
    {
        public int ProductId { get; set; }
        public string CategoryName { get; set; }
        public int CategoryID { get; set; }
        public string ProductName { get; set; }
        public string Description { get; set; }
        public decimal DiscountedPrice { get; set; }
        public decimal BasePrice { get; set; }
        public int StockQuantity { get; set; }
    }

}
