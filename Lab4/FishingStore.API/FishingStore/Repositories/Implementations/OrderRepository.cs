﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using FishingStoreDB.Data;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;

namespace FishingStoreDB.Repositories.Implementations
{
    public class OrderRepository : BaseRepository<Order>, IOrderRepository
    {
        public OrderRepository(FishingStoreDbContext context) : base(context) { }
        public IEnumerable<Order> GetOrderTracking()
        {
            return GetAll().AsNoTracking()
                .Include(b => b.OrderTracking);
        }

        public IEnumerable<Order> GetOrderDetails()
        {
            return GetAll().AsNoTracking()
                .Include(b => b.OrderDetails)
                .Where(b => b.OrderDetails.Count > 0);
        }
    }
}
