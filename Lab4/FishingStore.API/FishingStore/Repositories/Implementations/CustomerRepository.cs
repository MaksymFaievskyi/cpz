﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FishingStoreDB.Data;
using FishingStoreDB.Models;
using FishingStoreDB.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace FishingStoreDB.Repositories.Implementations
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository (FishingStoreDbContext context) : base(context) { }

        public IEnumerable<Customer> GetCustomerWithAddress()
        {
            return GetAll().AsNoTracking()
                .Include(b => b.CustomerAddresses)
                .Where(b => b.CustomerAddresses.Count > 0);
        }
    }
}
