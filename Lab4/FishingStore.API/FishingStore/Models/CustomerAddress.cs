﻿using System;
using System.Collections.Generic;

namespace FishingStoreDB.Models;

public partial class CustomerAddress
{
    public int AddressId { get; set; }

    public int? CustomerId { get; set; }

    public string? StreetAddress { get; set; }

    public string? City { get; set; }

    public int? PostalCode { get; set; }

    public string? Country { get; set; }

    public virtual Customer? Customer { get; set; }
}
