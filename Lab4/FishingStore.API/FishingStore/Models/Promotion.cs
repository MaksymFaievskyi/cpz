﻿using System;
using System.Collections.Generic;

namespace FishingStoreDB.Models;

public partial class Promotion
{
    public int PromotionId { get; set; }

    public int? ProductId { get; set; }

    public decimal? Discount { get; set; }

    public DateTime? StartDate { get; set; }

    public DateTime? EndDate { get; set; }

    public virtual Product? Product { get; set; }
}
