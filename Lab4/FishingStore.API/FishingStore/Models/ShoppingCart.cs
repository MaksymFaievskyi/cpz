﻿using System;
using System.Collections.Generic;

namespace FishingStoreDB.Models;

public partial class ShoppingCart
{
    public int ShoppingCartId { get; set; }

    public int CustomerId { get; set; }

    public int? ProductId { get; set; }

    public int? Quantity { get; set; }

    public virtual Customer Customer { get; set; } = null!;

    public virtual Product? Product { get; set; }
}
