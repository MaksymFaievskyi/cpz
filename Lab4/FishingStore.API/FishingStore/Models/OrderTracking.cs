﻿using System;
using System.Collections.Generic;

namespace FishingStoreDB.Models;

public partial class OrderTracking
{
    public int OrderId { get; set; }

    public string? Status { get; set; }

    public string? Description { get; set; }

    public DateTime? TrackingDate { get; set; }

    public virtual Order Order { get; set; } = null!;
}
