import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, Validators, ReactiveFormsModule, FormsModule } from "@angular/forms";
import { Customer } from 'src/app/features/models/customer';
import {RegexConstants} from "../../../constants/regex-constants";
import {FormsChecker} from "../../../helpers/forms-checker";
import {Constants} from "../../../constants/constants";
import {Confirmable} from "../../../../core/decorators/confirmable.decorator";

@Component({
  selector: 'app-add-edit-customer-modal',
  templateUrl: './add-edit-customer-modal.component.html',
  styleUrls: ['./add-edit-customer-modal.component.scss'],
})
export class AddEditCustomerModalComponent implements OnInit {
  customForm: FormGroup = {} as FormGroup;
  constructor(public dialogRef: MatDialogRef<AddEditCustomerModalComponent>,
              @Inject(MAT_DIALOG_DATA) public editedCustomer: Customer) {}

  ngOnInit(): void {
    this.customForm = new FormGroup({
      firstName: new FormControl('', [
        Validators.required,
        Validators.maxLength(Constants.maxWordLength),
        Validators.pattern(RegexConstants.onlyLetters)]),
      lastName: new FormControl('', [
        Validators.required,
        Validators.maxLength(Constants.maxWordLength),
        Validators.pattern(RegexConstants.onlyLetters)]),
      gender: new FormControl('', [
        Validators.required,
        Validators.maxLength(Constants.genderLength),
        Validators.pattern(RegexConstants.gender)]),
      age: new FormControl('', [
        Validators.required,
        Validators.min(0),
        Validators.max(100)]),
      email: new FormControl('', [
        Validators.maxLength(Constants.maxEmailLength),
        Validators.pattern(RegexConstants.email)]),
      phone: new FormControl('', [
        Validators.required,
        Validators.minLength(Constants.minPhoneLength),
        Validators.maxLength(Constants.maxWordLength),
        Validators.pattern(RegexConstants.phone)]),
    });
  }

  protected readonly FormsChecker = FormsChecker;
  protected readonly Constants = Constants;

  onSubmit() {
    if(this.customForm.valid) {
      this.dialogRef.close(this.readDataFromForm());
    }
  }

  @Confirmable({title: 'Are you sure?!', html: 'Do you want to leave unsaved changes?', icon: 'warning'})
  onExit() {
    this.dialogRef.close(undefined);
  }

  private readDataFromForm() {
    const id: number = this.editedCustomer.customerId;
    const firstName: string = this.customForm.controls['firstName'].value;
    const lastName: string = this.customForm.controls['lastName'].value;
    const gender : string = this.customForm.controls['gender'].value;
    const age : number = this.customForm.controls['age'].value;
    const email: string = this.customForm.controls['email'].value;
    const phone: string = this.customForm.controls['phone'].value;

    return new Customer(id, firstName, lastName, gender, age, email, phone);
  }
}
