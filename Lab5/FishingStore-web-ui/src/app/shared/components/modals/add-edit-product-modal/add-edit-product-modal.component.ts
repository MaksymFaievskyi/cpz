import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { Product } from 'src/app/features/models/product';
import {RegexConstants} from "../../../constants/regex-constants";
import {FormsChecker} from "../../../helpers/forms-checker";
import {Constants} from "../../../constants/constants";
import {Confirmable} from "../../../../core/decorators/confirmable.decorator";

@Component({
  selector: 'app-add-edit-product-modal',
  templateUrl: './add-edit-product-modal.component.html',
  styleUrls: ['./add-edit-product-modal.component.scss']
})

export class AddEditProductModalComponent {
  customForm: FormGroup = {} as FormGroup;
  constructor(public dialogRef: MatDialogRef<AddEditProductModalComponent>,
              @Inject(MAT_DIALOG_DATA) public editedProduct: Product) {
                console.log(editedProduct);
              }

  ngOnInit(): void {
    this.customForm = new FormGroup({
      categoryId: new FormControl('', [
        Validators.required,
        Validators.min(Constants.minCategoryId),
        Validators.max(Constants.maxCategoryId),
      ]),
      productName: new FormControl('', [
        Validators.required,
        Validators.maxLength(Constants.maxWordLength),
      ]),
      description: new FormControl('', ),
      basePrice: new FormControl('', [
        Validators.required,
        Validators.pattern(RegexConstants.price),]),
      stockQuantity: new FormControl('',[
        Validators.required,
        Validators.min(1),
        Validators.max(10000)]),
    });
  }

  protected readonly FormsChecker = FormsChecker;
  protected readonly Constants = Constants;

  onSubmit() {
    /* if(this.customForm.valid) { */
      this.dialogRef.close(this.readDataFromForm());
    /* } */
  }

  @Confirmable({title: 'Are you sure?!', html: 'Do you want to leave unsaved changes?', icon: 'warning'})
  onExit() {
    this.dialogRef.close(undefined);
  }

  private readDataFromForm() {
    const id: number = this.editedProduct.productId;
    const categoryId: number = this.customForm.controls['categoryId'].value;
    const productName: string = this.customForm.controls['productName'].value;
    const description : string = this.customForm.controls['description'].value;
    const basePrice: number = this.customForm.controls['basePrice'].value;
    const stockQuantity: number = this.customForm.controls['stockQuantity'].value;
    console.log("readDataFromForm" + new Product(id, categoryId, productName, description, basePrice, stockQuantity))

    return new Product(id, categoryId, productName, description, basePrice, stockQuantity);
  }
}