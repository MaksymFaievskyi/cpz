import { Component, Inject, OnInit } from '@angular/core';
import {MAT_DIALOG_DATA, MatDialogRef} from "@angular/material/dialog";
import {FormControl, FormGroup, Validators} from "@angular/forms";
import { Review } from 'src/app/features/models/review';
import {RegexConstants} from "../../../constants/regex-constants";
import {FormsChecker} from "../../../helpers/forms-checker";
import {Constants} from "../../../constants/constants";
import {Confirmable} from "../../../../core/decorators/confirmable.decorator";

@Component({
  selector: 'app-add-edit-review-modal',
  templateUrl: './add-edit-review-modal.component.html',
  styleUrls: ['./add-edit-review-modal.component.scss']
})
export class AddEditReviewModalComponent {
  customForm: FormGroup = {} as FormGroup;
  constructor(public dialogRef: MatDialogRef<AddEditReviewModalComponent>,
              @Inject(MAT_DIALOG_DATA) public editedReview: Review) {
                console.log(editedReview);
              }

  ngOnInit(): void {
    this.customForm = new FormGroup({
        productId: new FormControl('', [
        Validators.required ]),
        customerId: new FormControl('', [
        Validators.required ]),
        rating: new FormControl('', [
        Validators.required,
        Validators.min(Constants.minRating),
        Validators.max(Constants.maxRating)]),
        comment: new FormControl('', ),
        reviewDate: new FormControl('',
        [Validators.required]),
    });
  }

  protected readonly FormsChecker = FormsChecker;
  protected readonly Constants = Constants;

  onSubmit() {
    /* if(this.customForm.valid) { */
      this.dialogRef.close(this.readDataFromForm());
    /* } */
  }

  @Confirmable({title: 'Are you sure?!', html: 'Do you want to leave unsaved changes?', icon: 'warning'})
  onExit() {
    this.dialogRef.close(undefined);
  }

  private readDataFromForm() {
    const id: number = this.editedReview.reviewId;
    const productId: number = this.customForm.controls['productId'].value;
    const customerId: number = this.customForm.controls['customerId'].value;
    const rating : number = this.customForm.controls['rating'].value;
    const comment: string = this.customForm.controls['comment'].value;
    const reviewDate: string =  this.getCurrentDateTimeString();

    return new Review(id, productId, customerId, rating, comment, reviewDate);
  }

  private getCurrentDateTimeString() {
    const now = new Date();
    console.log(now.getTimezoneOffset)
    // Отримати рядок з датою та часом у форматі "YYYY-MM-DDTHH:mm:ss.sssZ"
    const dateString = now.toISOString();

    return dateString;
  }

}
