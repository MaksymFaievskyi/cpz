import { ComponentFixture, TestBed } from '@angular/core/testing';

import { AddEditReviewModalComponent } from './add-edit-review-modal.component';

describe('AddEditReviewModalComponent', () => {
  let component: AddEditReviewModalComponent;
  let fixture: ComponentFixture<AddEditReviewModalComponent>;

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [AddEditReviewModalComponent]
    });
    fixture = TestBed.createComponent(AddEditReviewModalComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
