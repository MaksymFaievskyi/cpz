export class Constants {
  public static maxWordLength = 20;
  public static maxEmailLength: 50;
  public static minPhoneLength = 8;
  public static genderLength = 1;
  public static minCategoryId = 1;
  public static maxCategoryId = 25;
  public static minRating = 1;
  public static maxRating = 5;
  public static inputLengthMessage: string = "Не більше нід 20 символів";
  public static inputGenderMessage: string = "Лише 'Ч' або 'Ж'";
  public static inputPhoneMessage: string = "Номер має бути типу 111-111-1111";
  public static inputAgeMessage: string = "Від 0 до 100";
  public static inputCategoryIdMessage: string = "Від 1 до 25";
  public static inputRatingMessage: string = "Від 1 до 5";
}
