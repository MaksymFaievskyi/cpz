export class CreateCustomerDto {
    constructor(
        public firstName: string = "",
        public lastName: string = "",
        public gender: string = "",
        public age: number = 0,
        public phone?: string,
        public email?: string,
    ) {}
}