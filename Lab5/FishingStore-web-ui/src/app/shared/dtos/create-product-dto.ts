export class CreateProductDto {
    constructor(
        public categoryId: number = 0,
        public productName: string = "",
        public description: string = "",
        public basePrice: number = 0,
        public stockQuantity: number = 0,
    ) {}
}