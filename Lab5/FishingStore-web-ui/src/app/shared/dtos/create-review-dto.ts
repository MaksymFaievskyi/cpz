export class CreateReviewDto {
    constructor(
        public productId: number = 0,
        public customerId: number = 0,
        public rating: number = 0,
        public comment: string = "",
        public reviewDate: string = "",
    ) {}
}