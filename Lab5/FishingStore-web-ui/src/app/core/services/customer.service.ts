import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment.dev";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {CreatedEntityDto} from "../../shared/dtos/created-entity-dto";
import {Customer} from "src/app/features/models/customer";
import { CreateCustomerDto } from 'src/app/shared/dtos/create-customer-dto'; 

@Injectable({
    providedIn: 'any'
})
export class CustomerService {
    constructor(private httpClient: HttpClient){ }

    private path: string = `${environment.apiUrl}Customer`;
    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    getCustomers() : Observable<Customer[]> {
        return this.httpClient.get<Customer[]>(`${this.path}/GetAllCustomers`);
    }

    addCustomer(createCustomerRequest: CreateCustomerDto) {
        return this.httpClient.post<CreatedEntityDto>(`${this.path}/CreateCustomer`, createCustomerRequest);
    }

    editCustomer(customerToEdit: Customer) {
        return this.httpClient.put<CreatedEntityDto>(`${this.path}/EditCustomer`, customerToEdit, this.httpOptions);
    }

    deleteCustomer(customerId: number) {
        return this.httpClient.delete(`${this.path}/DeleteCustomer/${customerId}`, {responseType: "text"});
    }
}