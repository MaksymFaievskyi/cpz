import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment.dev";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {CreatedEntityDto} from "../../shared/dtos/created-entity-dto";
import { Product } from '../../features/models/product';
import { ProductExpanded } from '../../features/models/productExpanded';
import { CreateProductDto } from 'src/app/shared/dtos/create-product-dto';

@Injectable({
    providedIn: 'any'
})
export class ProductService {
    constructor(private httpClient: HttpClient){ }

    private path: string = `${environment.apiUrl}Product`;
    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    getProducts() : Observable<ProductExpanded[]> {
        return this.httpClient.get<ProductExpanded[]>(`${this.path}/GetAllProducts`);
    }

    addProduct(createProductRequest: CreateProductDto) {
        return this.httpClient.post<CreatedEntityDto>(`${this.path}/CreateProduct`, createProductRequest);
    }

    editProduct(productToEdit: Product) {
        return this.httpClient.put<CreatedEntityDto>(`${this.path}/EditProduct`, productToEdit, this.httpOptions);
    }

    deleteProduct(productId: number) {
        return this.httpClient.delete(`${this.path}/DeleteProduct/${productId}`, {responseType: "text"});
    }
}