import { Injectable } from '@angular/core';
import {environment} from "../../../environments/environment.dev";
import {HttpClient, HttpHeaders} from "@angular/common/http";
import {Observable} from "rxjs";
import {CreatedEntityDto} from "../../shared/dtos/created-entity-dto";
import { Review } from '../../features/models/review';
import { CreateReviewDto } from 'src/app/shared/dtos/create-review-dto';

@Injectable({
    providedIn: 'any'
})
export class ReviewService {
    constructor(private httpClient: HttpClient){ }

    private path: string = `${environment.apiUrl}Review`;
    httpOptions = {
        headers: new HttpHeaders({'Content-Type': 'application/json'})
    }
    getReviews() : Observable<Review[]> {
        return this.httpClient.get<Review[]>(`${this.path}/GetAllReviews`);
    }

    addReview(createReviewRequest: CreateReviewDto) {
        return this.httpClient.post<CreatedEntityDto>(`${this.path}/CreateReview`, createReviewRequest);
    }

    editReview(reviewToEdit: Review) {
        return this.httpClient.put<CreatedEntityDto>(`${this.path}/EditReview`, reviewToEdit, this.httpOptions);
    }

    deleteReview(reviewId: number) {
        return this.httpClient.delete(`${this.path}/DeleteReview/${reviewId}`, {responseType: "text"});
    }
}