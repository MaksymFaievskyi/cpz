import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { HomeComponent } from './features/home/home.component';
import { ProductsPageComponent } from './features/products/products-page/products-page.component';
import { ReviewsPageComponent } from './features/reviews/reviews-page/reviews-page.component';
/* import {TripStatsPageComponent} from "./features/trip-stats/trip-stats-page/trip-stats-page.component";
import {TripsPageComponent} from "./features/trips/trips-page/trips-page.component";
import {TicketsPageComponent} from "./features/tickets/tickets-page/tickets-page.component";
import {AboutPageComponent} from "./shared/components/about-page/about-page.component";
import {aboutGuard} from "./core/guards/about.guard"; */
import { AboutPageComponent } from './shared/components/about-page/about-page.component';
import { aboutGuard } from './core/guards/about.guard';

const routes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'customers', loadChildren: () => import('./features/customers/customers.module').then(m => m.CustomersModule) },
  { path: 'products', loadChildren: () => import('./features/products/products.module').then(m => m.ProductsModule) },
  { path: 'reviews', loadChildren: () => import('./features/reviews/reviews.module').then(m => m.ReviewModule) },
  { path: 'about', component: AboutPageComponent, canActivate: [aboutGuard] },
  { path: '**', redirectTo: ''}
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
