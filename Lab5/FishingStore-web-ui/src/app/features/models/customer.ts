export class Customer {
    customerId: number = 0;
    firstName: string = "";
    lastName: string = "";
    gender: string = "";
    age: number = 0;
    email?: string
    phone?: string

    constructor(customerId: number, firstName: string, lastName: string, gender: string, age: number, email: string, phone: string) {
        this.customerId = customerId;
        this.firstName = firstName;
        this.lastName = lastName;
        this.gender = gender;
        this.age = age;
        this.email = email;
        this.phone = phone;
    }
}