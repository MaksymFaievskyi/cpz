export class Product {
    productId: number = 0;
    categoryId: number = 0;
    productName: string = "";
    description: string = "";
    basePrice: number = 0;
    stockQuantity: number = 0

    constructor(productId: number, categoryId: number, productName: string, description: string, basePrice: number, stockQuantity: number) {
        this.productId = productId;
        this.categoryId = categoryId;
        this.productName = productName;
        this.description = description;
        this.basePrice = basePrice;
        this.stockQuantity = stockQuantity;
    }
}