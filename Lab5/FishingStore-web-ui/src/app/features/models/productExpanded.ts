export class ProductExpanded {
    productId: number = 0;
    categoryId: number = 0;
    categoryName: string = "";
    productName: string = "";
    description: string = "";
    discountedPrice: number = 0;
    basePrice: number = 0;
    stockQuantity: number = 0

    constructor(productId: number, categoryId: number,categoryName: string, productName: string, description: string, discountedPrice: number, basePrice: number, stockQuantity: number) {
        this.productId = productId;
        this.categoryId = categoryId;
        this.categoryName = categoryName;
        this.productName = productName;
        this.description = description;
        this.discountedPrice = discountedPrice;
        this.basePrice = basePrice;
        this.stockQuantity = stockQuantity;
    }
}