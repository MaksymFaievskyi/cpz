export class Review {
    reviewId: number = 0;
    productId: number = 0;
    customerId: number = 0;
    rating: number = 0;
    comment: string = "";
    reviewDate: string = "";

    constructor(reviewId: number, productId: number, customerId: number, rating: number, comment: string, reviewDate: string) {
        this.reviewId = reviewId;
        this.productId = productId;
        this.customerId = customerId;
        this.rating = rating;
        this.comment = comment;
        this.reviewDate = reviewDate;
    }
}