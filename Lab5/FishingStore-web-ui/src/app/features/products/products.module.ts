import { NgModule } from '@angular/core';
import {CommonModule} from '@angular/common';
import { ProductsPageComponent } from './products-page/products-page.component';
import { ProductsRoutingModule } from './products-routing.module';
import {MatButtonModule} from "@angular/material/button";
import {MatIconModule} from "@angular/material/icon";
import {MatPaginatorModule} from "@angular/material/paginator";
import {MatDialogModule} from "@angular/material/dialog";
import {MatTableModule} from "@angular/material/table";
import {NavbarComponent} from "../../shared/components/navbar/navbar.component";
import {HoverDirective} from "../../shared/directives/hover.directive";
import {MatFormFieldModule} from "@angular/material/form-field";
import {MatInputModule} from "@angular/material/input";
import {ReactiveFormsModule} from "@angular/forms";
import { AddEditProductModalComponent } from 'src/app/shared/components/modals/add-edit-product-modal/add-edit-product-modal.component';

@NgModule({
  declarations: [ ProductsPageComponent, AddEditProductModalComponent],
  exports: [
  ],
  imports: [
    CommonModule,
    ProductsRoutingModule,
    MatButtonModule,
    MatIconModule,
    MatPaginatorModule,
    MatDialogModule,
    MatTableModule,
    NavbarComponent,
    HoverDirective,
    MatFormFieldModule, MatInputModule, ReactiveFormsModule
  ],
})
export class ProductsModule { }
