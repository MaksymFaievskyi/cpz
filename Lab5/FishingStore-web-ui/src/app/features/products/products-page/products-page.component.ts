import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import { Product } from '../../models/product';
import { ProductExpanded } from '../../models/productExpanded';
import {MatDialog} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import { ProductService } from '../../../core/services/product.service';
import { AddEditProductModalComponent } from 'src/app/shared/components/modals/add-edit-product-modal/add-edit-product-modal.component';
import { CreateProductDto } from 'src/app/shared/dtos/create-product-dto';
import {Confirmable} from "../../../core/decorators/confirmable.decorator";
import {HttpErrorResponse} from "@angular/common/http";
import {CreatedEntityDto} from "../../../shared/dtos/created-entity-dto";

@Component({
  selector: 'app-products-page',
  templateUrl: './products-page.component.html',
  styleUrls: ['./products-page.component.scss']
})
export class ProductsPageComponent {
  products: ProductExpanded[] = [];
  tableSource: MatTableDataSource<ProductExpanded> = new MatTableDataSource<ProductExpanded>();
  displayedColumns: string[] = ['Product ID', 'Category ID', 'Category Name', 'Product Name','Description', 'Discounted Price', 'Base Price', 'Stock Quantity','Actions'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = {} as MatPaginator;

  constructor(private productService: ProductService,
              public dialog: MatDialog,
              private toastrService: ToastrService) {
  }
  ngOnInit() {
    this.load();
  }

  add() {
    const dialogRef = this.dialog.open(AddEditProductModalComponent, {
      data: new Product(0, 0, "", "",0 , 0)
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log("Created value", result);
        let createRequest: CreateProductDto = new CreateProductDto(
          result.categoryId,
          result.productName,
          result.description,
          result.basePrice,
          result.stockQuantity);

        this.productService.addProduct(createRequest).subscribe({
          next: (result: CreatedEntityDto) => {
            this.toastrService.success(`Product №${result.id} was created!`, "Success!");
            window.location.reload();
          },
          error: (httpError: HttpErrorResponse) => {
            const errorValue: any | null = httpError.error;
            const errorCode: number = httpError.status;
            console.error(`Endpoint returned error ${errorValue} with status code ${errorCode}`);
          }
        });
      } else {
        this.toastrService.warning("Request body is empty", "Attention!");
      }
    });
  }

  edit(productToEdit: Product) {
    const dialogRef = this.dialog.open(AddEditProductModalComponent, {
      data: productToEdit
    });

    dialogRef.afterClosed().subscribe(result => {
      productToEdit = result;
      console.log("Edited value", productToEdit);
      if (productToEdit) {
        this.productService.editProduct(productToEdit).subscribe({
          next: (createdEntity: CreatedEntityDto) => {
          this.toastrService.success(`Product №${createdEntity.id} was edited!`, "Success!");
          window.location.reload();
        },
          error: (httpError: HttpErrorResponse) => {
            const errorValue: any | null = httpError.error;
            const errorCode: number = httpError.status;
            console.error(`Endpoint returned error ${errorValue} with status code ${errorCode}`);
          }
        });
      }
      else {
        this.toastrService.warning("Request body is empty", "Attention!");
      }
    });
  }

  @Confirmable({ title: 'Are you sure?!', html: 'Do you want to delete this product?', icon: 'warning'})
  delete(productId: number) {
      this.productService.deleteProduct(productId).subscribe(message => {
          this.toastrService.success(message, "Success");
          this.load();
      });
  }

  private load(): void {
    this.productService.getProducts().subscribe((result: ProductExpanded[]) => {
      this.products = result;
      console.log(this.products);
      this.tableSource = new MatTableDataSource<ProductExpanded>(this.products);
      this.tableSource.paginator = this.paginator;
    });
  }
}
