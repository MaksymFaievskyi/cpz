import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ProductsPageComponent } from './products-page/products-page.component';
import { AddEditProductModalComponent } from 'src/app/shared/components/modals/add-edit-product-modal/add-edit-product-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ProductsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ProductsRoutingModule { }