import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ReviewsPageComponent } from './reviews-page/reviews-page.component';
import { AddEditReviewModalComponent } from 'src/app/shared/components/modals/add-edit-review-modal/add-edit-review-modal.component';

const routes: Routes = [
  {
    path: '',
    component: ReviewsPageComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ReviewRoutingModule { }