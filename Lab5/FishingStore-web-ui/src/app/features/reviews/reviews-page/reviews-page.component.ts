import { Component, OnInit, ViewChild } from '@angular/core';
import {MatPaginator} from "@angular/material/paginator";
import {MatTableDataSource} from "@angular/material/table";
import { Review } from '../../models/review';
import {MatDialog} from "@angular/material/dialog";
import {ToastrService} from "ngx-toastr";
import { ReviewService } from '../../../core/services/review.service';
import { AddEditReviewModalComponent } from 'src/app/shared/components/modals/add-edit-review-modal/add-edit-review-modal.component';
import { CreateReviewDto } from 'src/app/shared/dtos/create-review-dto';
import {Confirmable} from "../../../core/decorators/confirmable.decorator";
import {HttpErrorResponse} from "@angular/common/http";
import {CreatedEntityDto} from "../../../shared/dtos/created-entity-dto";

/* reviewId: number = 0;
productId: number = 0;
customerId: number = 0;
rating: number = 0;
comment: string = "";
reviewDate: string = ""; */


@Component({
  selector: 'app-reviews-page',
  templateUrl: './reviews-page.component.html',
  styleUrls: ['./reviews-page.component.scss']
})
export class ReviewsPageComponent {
  reviews: Review[] = [];
  tableSource: MatTableDataSource<Review> = new MatTableDataSource<Review>();
  displayedColumns: string[] = ['Review ID', 'Product ID', 'Customer ID', 'Rating', 'Comment', 'Review Date', 'Actions'];
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator = {} as MatPaginator;

  constructor(private reviewService: ReviewService,
              public dialog: MatDialog,
              private toastrService: ToastrService) {
  }
  ngOnInit() {
    this.load();
  }

  add() {
    const dialogRef = this.dialog.open(AddEditReviewModalComponent, {
      data: new Review(0, 0, 0, 0, "" , "")
    });

    dialogRef.afterClosed().subscribe(result => {
      if(result) {
        console.log("Created value", result);
        let createRequest: CreateReviewDto = new CreateReviewDto(
          result.productId,
          result.customerId,
          result.rating,
          result.comment,
          result.reviewDate);

        this.reviewService.addReview(createRequest).subscribe({
          next: (result: CreatedEntityDto) => {
            this.toastrService.success(`Review №${result.id} was created!`, "Success!");
            window.location.reload();
          },
          error: (httpError: HttpErrorResponse) => {
            const errorValue: any | null = httpError.error;
            const errorCode: number = httpError.status;
            console.error(`Endpoint returned error ${errorValue} with status code ${errorCode}`);
          }
        });
      } else {
        this.toastrService.warning("Request body is empty", "Attention!");
      }
    });
  }

  edit(reviewToEdit: Review) {
    const dialogRef = this.dialog.open(AddEditReviewModalComponent, {
      data: reviewToEdit
    });

    dialogRef.afterClosed().subscribe(result => {
      reviewToEdit = result;
      console.log("Edited value", reviewToEdit);
      if (reviewToEdit) {
        this.reviewService.editReview(reviewToEdit).subscribe({
          next: (createdEntity: CreatedEntityDto) => {
          this.toastrService.success(`Review №${createdEntity.id} was edited!`, "Success!");
          window.location.reload();
        },
          error: (httpError: HttpErrorResponse) => {
            const errorValue: any | null = httpError.error;
            const errorCode: number = httpError.status;
            console.error(`Endpoint returned error ${errorValue} with status code ${errorCode}`);
          }
        });
      }
      else {
        this.toastrService.warning("Request body is empty", "Attention!");
      }
    });
  }

  @Confirmable({ title: 'Are you sure?!', html: 'Do you want to delete this review?', icon: 'warning'})
  delete(reviewId: number) {
      this.reviewService.deleteReview(reviewId).subscribe(message => {
          this.toastrService.success(message, "Success");
          this.load();
      });
  }

  private load(): void {
    this.reviewService.getReviews().subscribe((result: Review[]) => {
      this.reviews = result;
      console.log(this.reviews);
      this.tableSource = new MatTableDataSource<Review>(this.reviews);
      this.tableSource.paginator = this.paginator;
    });
  }
}
