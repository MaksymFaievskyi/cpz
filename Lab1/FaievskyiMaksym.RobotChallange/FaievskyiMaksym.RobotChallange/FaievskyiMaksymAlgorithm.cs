using Robot.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FaievskyiMaksym.RobotChallange
{
	public class FaievskyiMaksymAlgorithm : IRobotAlgorithm
	{
		public string Author
		{
			get
			{
				return "Faievskyi Maksym";
			}
		}
		 

		public FaievskyiMaksymAlgorithm() { }

		public RobotCommand DoStep(IList<Robot.Common.Robot> robots, int robotToMoveIndex, Map map)
		{
			RobotCommand mainCommand = null;
			Robot.Common.Robot item = robots[robotToMoveIndex];
			EnergyStation energyStation = FindStation(item, map, robots);
			Position goodCell = GetGoodCell(energyStation, item, robots);

			if (goodCell != null)
			{
				if (goodCell != item.Position)
				{
					if (FindDistance(goodCell, item.Position) <= item.Energy)
					{
						MoveCommand moveCommand = new MoveCommand();
						moveCommand.NewPosition = goodCell;
						mainCommand = moveCommand;
					}
					else
					{
						int x = goodCell.X - item.Position.X;
						int y = goodCell.Y - item.Position.Y;
						if (x > 0)
						{
							x = 1;
						}
						else if (x < 0)
						{
							x = -1;
						}
						if (y > 0)
						{
							y = 1;
						}
						else if (y < 0)
						{
							y = -1;
						}
						Position position = new Position(item.Position.X + x, item.Position.Y + y);
						MoveCommand moveCommand1 = new MoveCommand();
						moveCommand1.NewPosition = position;
						mainCommand = moveCommand1;
					}
				}
				else
				{
					if (item.Energy <= 150 || robots.Count((Robot.Common.Robot r) => r.OwnerName == "Faievskyi Maksym") >= 100)
					{
						mainCommand = new CollectEnergyCommand();
					}
					else
					{
						CreateNewRobotCommand createNewRobotCommand = new CreateNewRobotCommand();
						createNewRobotCommand.NewRobotEnergy = 25;
						mainCommand = createNewRobotCommand;
					}
				}
			}
			else
			{
				goodCell = energyStation.Position;
			}

			return mainCommand;
		}

		public int FindDistance(Position a, Position b)
		{
			double x = (double)((a.X - b.X) * (a.X - b.X) + (a.Y - b.Y) * (a.Y - b.Y));
			return (int)Math.Sqrt(x);
		}

		public EnergyStation FindStation(Robot.Common.Robot movingRobot, Map map, IList<Robot.Common.Robot> robots)
		{
			EnergyStation energyStation = null;
			int num = int.MaxValue;
			foreach (EnergyStation station in map.Stations)
			{
				Position goodCell = GetGoodCell(station, movingRobot, robots);
				if (goodCell != null)
				{
					int num1 = FindDistance(goodCell, movingRobot.Position);
					if (num1 < num)
					{
						num = num1;
						energyStation = station;
					}
				}
			}
			return energyStation;
		}

		public Position GetGoodCell(EnergyStation station, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
		{
			Position position;
			int num = 0;
			Position position1 = null;
			int num1 = int.MaxValue;
			List<Robot.Common.Robot> listMyRobots = (
				from r in robots
				where r.OwnerName == "Faievskyi Maksym"
				select r).ToList();
			int num2 = -2;
			while (true)
			{
				if (num2 <= 2)
				{
					for (int i = -2; i <= 2; i++)
					{
						int x = station.Position.X + num2;
						int y = station.Position.Y + i;
						Position position2 = new Position(x, y);
						if (position2.X < 0 || position2.Y < 0 || position2.X > 99 ? false : position2.Y <= 99)
						{
							if (!IsCellFree(position2, movingRobot, listMyRobots))
							{
								//number my robots around tha station
								num++;
							}
							if (num < station.Energy / 300 + 1)
							{
								int num3 = FindDistance(movingRobot.Position, position2);
								if (num3 < num1)
								{
									num1 = num3;
									position1 = position2;
								}
							}
							else
							{
								position = null;
								return position;
							}
						}
					}
					num2++;
				}
				else
				{
					position = position1;
					break;
				}
			}
			return position;
		}

		public bool IsCellFree(Position cell, Robot.Common.Robot movingRobot, IList<Robot.Common.Robot> robots)
		{
			bool flag = (
				from robot in robots
				where (object)robot != (object)movingRobot
				select robot).All((Robot.Common.Robot robot) => robot.Position != cell);
			return flag;
		}
	}
}
