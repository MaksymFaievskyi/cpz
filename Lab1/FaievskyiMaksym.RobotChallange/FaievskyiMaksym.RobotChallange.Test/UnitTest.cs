﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using Robot.Common;
using FaievskyiMaksym.RobotChallange;
using System.Collections.Generic;
using System.Linq;

namespace FaievskyiMaksym.RobotChallange.Test
{
    [TestClass]
    public class UnitTest1
    {
        [TestMethod]
        public void TestAuthor()
        {
            Assert.AreEqual("Faievskyi Maksym", new FaievskyiMaksymAlgorithm().Author);
        }

        [TestMethod]
        public void TestFindDistance()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var position1 = new Position(5, 6);
            var position2 = new Position(10, 8);

            // Act
            int distance = algorithm.FindDistance(position1, position2);

            // Assert
            Assert.AreEqual(5, distance);
        }

        [TestMethod]
        public void Test_FindStation()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var myRobot = new Robot.Common.Robot {Position = new Position(5,5) };

            var energyStation1 = new EnergyStation { Position = new Position(2, 2)};
            var energyStation2 = new EnergyStation { Position = new Position(7, 4)};
            var energyStation3 = new EnergyStation { Position = new Position(3, 8)};
            var energyStation4 = new EnergyStation { Position = new Position(10, 9)};

            var map = new Map();

            map.Stations = new List<EnergyStation> { energyStation1, energyStation2, energyStation3, energyStation4 };

            var robots = new List<Robot.Common.Robot> { 
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(55, 72) },
                new Robot.Common.Robot { OwnerName = "OtherOwner", Position = new Position(50, 50) } 
            };

            // Act
            EnergyStation station = algorithm.FindStation(myRobot, map, robots);
            var x = station.Position.X;
            var y = station.Position.Y;

            // Assert
            Assert.AreEqual(x, 7);
            Assert.AreEqual(y, 4);
        }

        [TestMethod]
        public void Test_GetGoodCell()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var station = new EnergyStation { Position = new Position(3, 3), Energy = 0 };
            var movingRobot = new Robot.Common.Robot { Position = new Position(6, 6) };
            var robots = new List<Robot.Common.Robot>
            {
                movingRobot,
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(55, 72) },
                new Robot.Common.Robot { OwnerName = "OtherOwner", Position = new Position(50, 50) }
            };

            // Act
            Position goodCell = algorithm.GetGoodCell(station, movingRobot, robots);

            // Assert
            Assert.AreEqual(new Position(5,5), goodCell);
        }


        [TestMethod]
        public void Test_IsCellFree()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var cell = new Position(4, 4);
            var robot = new Robot.Common.Robot
            {
                Position = new Position(5, 5)
            };

            var robot1 = new Robot.Common.Robot { Position = new Position(1, 1) };
            var robot2 = new Robot.Common.Robot { Position = new Position(3, 3) };
            var robot3 = new Robot.Common.Robot { Position = new Position(8, 8) };

            var robots = new List<Robot.Common.Robot> { robot1, robot2, robot3 };

            // Act
            bool isCellFree = algorithm.IsCellFree(cell, robot, robots);

            // Assert
            Assert.IsTrue(isCellFree);
        }

        [TestMethod]
        public void Test_DoStep_CollectEnergy()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var map = new Map();
            var energyStation = new EnergyStation { Position = new Position(2, 1), Energy = 10000 };
            var robots = new List<Robot.Common.Robot> {
                new Robot.Common.Robot { Position = new Position(1, 1), Energy = 10 },
                new Robot.Common.Robot { Position = new Position(5, 7), Energy = 20 },
                new Robot.Common.Robot { Position = new Position(3, 1), Energy = 30 }
            };
            map.Stations = new List<EnergyStation> { energyStation };

            // Act
            RobotCommand command = algorithm.DoStep(robots, 0, map);

            // Assert
            Assert.IsInstanceOfType(command, typeof(CollectEnergyCommand));
        }

        [TestMethod]
        public void Test_DoStep_CreateNewRobot()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var robot = new Robot.Common.Robot
            {
                Position = new Position(1, 3),
                Energy = 10000,
                OwnerName = "Faievskyi Maksym" 
            };
            var map = new Map();
            map.Stations = new List<EnergyStation> {
                new EnergyStation { Position = new Position(2, 2), Energy = 10000 },
            };
            var robots = new List<Robot.Common.Robot> { robot };

            // Act
            RobotCommand command = algorithm.DoStep(robots, 0, map);

            // Assert
            Assert.IsInstanceOfType(command, typeof(CreateNewRobotCommand));
        }

        [TestMethod]
        public void Test_DoStep_MoveCommand()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var robot = new Robot.Common.Robot
            {
                Position = new Position(5, 5),
                Energy = 1000, 
                OwnerName = "Faievskyi Maksym" 
            };
            var map = new Map();
            map.Stations = new List<EnergyStation> {
                new EnergyStation { Position = new Position(2, 2), Energy = 10000 },
            };
            var robots = new List<Robot.Common.Robot> { robot };

            // Act
            RobotCommand command = algorithm.DoStep(robots, 0, map);

            // Assert
            Assert.IsInstanceOfType(command, typeof(MoveCommand));
        }

        [TestMethod]
        public void Test_GetGoodCell_DontGo()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var station = new EnergyStation { Position = new Position(3, 3), Energy = 500 };
            var movingRobot = new Robot.Common.Robot { Position = new Position(5, 5) };
            var robots = new List<Robot.Common.Robot>
            {
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(2, 1) },
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(2, 3) },
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(2, 4) },

            };

            // Act
            Position goodCell = algorithm.GetGoodCell(station, movingRobot, robots);

            // Assert
            Assert.AreEqual(null, goodCell);
        }

        [TestMethod]
        public void Test_DontCanFindNormalStation()
        {
            // Arrange
            var algorithm = new FaievskyiMaksymAlgorithm();
            var robot = new Robot.Common.Robot
            {
                Position = new Position(5, 5),
                Energy = 0, 
                OwnerName = "Faievskyi Maksym" 
            };
            var map = new Map();
            map.MinPozition = new Position(0,0);
            map.MinPozition = new Position(100, 100);
            map.Stations = new List<EnergyStation> {
                new EnergyStation { Position = new Position(3, 3), Energy = 500 },
            };
            var robots = new List<Robot.Common.Robot> { 
                robot,
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(2, 2) },
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(2, 3) },
                new Robot.Common.Robot { OwnerName = "Faievskyi Maksym", Position = new Position(2, 4) },

            };

            // Act
            var station = algorithm.FindStation(robot, map, robots);

            // Assert
            Assert.IsNull(station);
        }
    }
}