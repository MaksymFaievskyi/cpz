﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.Data.Entity;
using Model;

namespace View
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        Model.StoreContext _dbConteiner;
        public MainWindow()
        {
            
            InitializeComponent();
            _dbConteiner = new StoreContext();
            CustomersTable.ItemsSource = _dbConteiner.Customers.ToList();
            OrdersTable.ItemsSource = _dbConteiner.Orders.ToList();
            ProductsTable.ItemsSource = _dbConteiner.Products.ToList();
            SellersTable.ItemsSource = _dbConteiner.Sellers.ToList();
        }

        //Customer table
        private void CustomersTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer != null)
            {
                FirstNameTextBox.Text = selectedCustomer.FirstName;
                LastNameTextBox.Text = selectedCustomer.LastName;
                AgeTextBox.Text = selectedCustomer.Age.ToString();
                GenderTextBox.Text = selectedCustomer.Gender;
                AddressTextBox.Text = selectedCustomer.Address;
                EmailTextBox.Text = selectedCustomer.Email;
                PhoneNumberTextBox.Text = selectedCustomer.Phone;
            }
        }

        private void AddCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            var newCustomer = new Customer
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                Age = int.Parse(AgeTextBox.Text),
                Gender = GenderTextBox.Text,
                Address = AddressTextBox.Text,
                Email = EmailTextBox.Text,
                Phone = PhoneNumberTextBox.Text
            };
            _dbConteiner.Customers.Add(newCustomer);
            _dbConteiner.SaveChanges();
            CustomersTable.ItemsSource = _dbConteiner.Customers.ToList();
        }

        private void EditCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer != null)
            {
                selectedCustomer.FirstName = FirstNameTextBox.Text;
                selectedCustomer.LastName = LastNameTextBox.Text;
                selectedCustomer.Address = AddressTextBox.Text;
                selectedCustomer.Gender = GenderTextBox.Text;
                selectedCustomer.Age = int.Parse(AgeTextBox.Text);
                selectedCustomer.Email = EmailTextBox.Text;
                selectedCustomer.Phone = PhoneNumberTextBox.Text;

                _dbConteiner.Customers.Update(selectedCustomer);
                _dbConteiner.SaveChanges();
                CustomersTable.ItemsSource = _dbConteiner.Customers.ToList();
            }
        }

        private void DeleteCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer != null)
            {
                _dbConteiner.Customers.Remove(selectedCustomer);
                _dbConteiner.SaveChanges();
                CustomersTable.ItemsSource = _dbConteiner.Customers.ToList();
            }
        }
        //Product table
        private void ProductsTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedProduct = ProductsTable.SelectedItem as Product;

            if (selectedProduct != null)
            {
                ProductNameTextBox.Text = selectedProduct.ProductName;
                DescriptionTextBox.Text = selectedProduct.Description;
                PriceTextBox.Text = selectedProduct.Price.ToString();

            }
        }

        private void AddProductButton_Click(object sender, RoutedEventArgs e)
         {
            var newProduct = new Product
            {
                ProductName = ProductNameTextBox.Text,
                Description = DescriptionTextBox.Text,
                Price = decimal.Parse(PriceTextBox.Text),
            };
            _dbConteiner.Products.Add(newProduct);
            _dbConteiner.SaveChanges();
            ProductsTable.ItemsSource = _dbConteiner.Products.ToList();
        }

        private void EditProductButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedProduct = ProductsTable.SelectedItem as Product;
            if (selectedProduct != null)
            {
                selectedProduct.ProductName = ProductNameTextBox.Text;
                selectedProduct.Description = DescriptionTextBox.Text;
                selectedProduct.Price = decimal.Parse(PriceTextBox.Text);



                _dbConteiner.Products.Update(selectedProduct);
                _dbConteiner.SaveChanges();
                ProductsTable.ItemsSource = _dbConteiner.Products.ToList();
            }
        }

        private void DeleteProductButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedProduct = ProductsTable.SelectedItem as Product;
            if (selectedProduct != null)
            {
                _dbConteiner.Products.Remove(selectedProduct);
                _dbConteiner.SaveChanges();
                ProductsTable.ItemsSource = _dbConteiner.Products.ToList();

            }
        }

        //Seller table
        private void SellersTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedSeller = SellersTable.SelectedItem as Seller;
            if (selectedSeller != null)
            {
                SellerNameTextBox.Text = selectedSeller.SellerName;
                PhoneTextBox.Text = selectedSeller.Phone;
                SellerEmailTextBox.Text = selectedSeller.Email;
                RatingTextBox.Text = selectedSeller.Rating.ToString();
            }
        } 

        private void AddSellerButton_Click(object sender, RoutedEventArgs e)
        {
            var newSeller = new Seller
            {
                SellerName = SellerNameTextBox.Text,
                Phone = PhoneTextBox.Text,
                Email = SellerEmailTextBox.Text,
                Rating = int.Parse(RatingTextBox.Text)
            };
            _dbConteiner.Sellers.Add(newSeller);
            _dbConteiner.SaveChanges();
            SellersTable.ItemsSource = _dbConteiner.Sellers.ToList();
        }

        private void EditSellerButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedSeller = SellersTable.SelectedItem as Seller;
            if (selectedSeller != null)
            {
                selectedSeller.SellerName = SellerNameTextBox.Text;
                selectedSeller.Phone = PhoneTextBox.Text;
                selectedSeller.Email = SellerEmailTextBox.Text;
                selectedSeller.Rating = int.Parse(RatingTextBox.Text);


                _dbConteiner.Sellers.Update(selectedSeller);
                _dbConteiner.SaveChanges();
                SellersTable.ItemsSource = _dbConteiner.Sellers.ToList();
            }
        }

        private void DeleteSellerButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedSeller = SellersTable.SelectedItem as Seller;
            if (selectedSeller != null)
            {
                _dbConteiner.Sellers.Remove(selectedSeller);
                _dbConteiner.SaveChanges();
                SellersTable.ItemsSource = _dbConteiner.Sellers.ToList();
            }
        }

        //Order table
        private void OrdersTable_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder != null)
            {
                CustomerIdTextBox.Text = selectedOrder.CustomerID.ToString();
                SellerIdTextBox.Text = selectedOrder.SellerID.ToString();
                ProductIdTextBox.Text = selectedOrder.ProductID.ToString();
                QuantityTextBox.Text = selectedOrder.Quantity.ToString();
                OrderDateTextBox.Text = selectedOrder.OrderDate.ToString();
                TotalPriceTextBox.Text = selectedOrder.TotalPrice.ToString();

            }
        }

        private void AddOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var newOrder = new Order
            {
                CustomerID = int.Parse(CustomerIdTextBox.Text),
                SellerID = int.Parse(SellerIdTextBox.Text),
                ProductID = int.Parse(ProductIdTextBox.Text),
                Quantity = int.Parse(QuantityTextBox.Text),
                OrderDate = DateTime.Parse(OrderDateTextBox.Text),
                TotalPrice = decimal.Parse(TotalPriceTextBox.Text),
            };
            _dbConteiner.Orders.Add(newOrder);
            _dbConteiner.SaveChanges();
            OrdersTable.ItemsSource = _dbConteiner.Orders.ToList();

        }
        private void EditOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder != null)
            {
                selectedOrder.CustomerID = int.Parse(CustomerIdTextBox.Text);
                selectedOrder.SellerID = int.Parse(SellerIdTextBox.Text);
                selectedOrder.ProductID = int.Parse(ProductIdTextBox.Text);
                selectedOrder.Quantity = int.Parse(QuantityTextBox.Text);
                selectedOrder.OrderDate = DateTime.Parse(OrderDateTextBox.Text);
                selectedOrder.TotalPrice = decimal.Parse(TotalPriceTextBox.Text);

                _dbConteiner.Orders.Update(selectedOrder);
                _dbConteiner.SaveChanges();
                OrdersTable.ItemsSource = _dbConteiner.Orders.ToList();
            }
        }

        private void DeleteOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder != null)
            {
                _dbConteiner.Orders.Remove(selectedOrder);
                _dbConteiner.SaveChanges();
                OrdersTable.ItemsSource = _dbConteiner.Orders.ToList();
            }
        }
    }
}
