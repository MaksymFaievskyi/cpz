﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity;

namespace Model
{
    public class Seller
    {
        public int SellerID { get; set; }
        public string SellerName { get; set; }
        public string Phone { get; set; }
        public string Email { get; set; }
        public int Rating { get; set; }

        // Навігаційні властивості для замовлень, які містять цього продавця
        public virtual ICollection<Order> Orders { get; set; } = new List<Order>();
    }
}
