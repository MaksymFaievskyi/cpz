﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KPZ_Lab_3_DB_First.Data;
using KPZ_Lab_3_DB_First.Models;
using KPZ_Lab_3_DB_First.Repositories.Interfaces;

namespace KPZ_Lab_3_DB_First.Repositories.Implementations
{
    internal class ReviewRepository : BaseRepository<Review>, IReviewRepository
    {
        public ReviewRepository(FishingStoreDbContext context) : base(context) { }



    }
}
