﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using KPZ_Lab_3_DB_First.Data;
using KPZ_Lab_3_DB_First.Models;
using KPZ_Lab_3_DB_First.Repositories.Interfaces;

namespace KPZ_Lab_3_DB_First.Repositories.Implementations
{
    /*    public class ProductRepository : BaseRepository<Product>, IProductRepository
        {
            public ProductRepository(FishingStoreDbContext context) : base(context) { }

            public IEnumerable<Product> GetProductWithCategory()
            {
                return GetAll().AsNoTracking()
                    .Include(b => b.Category);
            }
        }*/
    public class ProductRepository : BaseRepository<Product>, IProductRepository
    {
        public ProductRepository(FishingStoreDbContext context) : base(context) { }

        public IEnumerable<ProductWithCategory> GetProductWithCategory()
        {
            var productsWithCategory = GetAll()
                .Include(b => b.Category)
                .Select(product => new ProductWithCategory
                {
                    ProductId = product.ProductId,
                    CategoryName = product.Category != null ? product.Category.CategoryName : null,
                    CategoryID = product.Category != null ? product.Category.CategoryId : 0,
                    ProductName = product.ProductName,
                    Description = product.Description,
                    Price = product.Price ?? 0, // Додайте операцію ?? 0 для приведення decimal? до decimal
                    StockQuantity = product.StockQuantity ?? 0 // Додайте операцію ?? 0 для приведення int? до int
                });

            return productsWithCategory;
        }
        public int GetCategoryByName(string name)
        {
            var product = GetAll()
                .Include(b => b.Category)
                .FirstOrDefault(b => b.Category.CategoryName == name);

            if (product != null && product.Category != null)
            {
                // Повернути ідентифікатор категорії
                return product.Category.CategoryId;
            }
            else
            {
                // Якщо категорія не знайдена, можна повернути якийсь значення за замовчуванням
                // або сигналізувати про помилку
                return -1; // Наприклад, -1 як код помилки, але це може бути змінене на щось інше.
            }
        }
        public string GetCategoryNameById(int categoryId)
        {
            // Знайти категорію за ідентифікатором
            var category = GetAll()
                .Include(b => b.Category)
                .FirstOrDefault(c => c.CategoryId == categoryId);

            if (category != null)
            {
                // Повернути назву категорії
                return category.Category.CategoryName;
            }
            else
            {
                // Якщо категорія не знайдена, можна повернути якийсь значення за замовчуванням
                // або сигналізувати про помилку
                return string.Empty; // Наприклад, пустий рядок як значення за замовчуванням, але це може бути змінене на щось інше.
            }
        }
    }
}
