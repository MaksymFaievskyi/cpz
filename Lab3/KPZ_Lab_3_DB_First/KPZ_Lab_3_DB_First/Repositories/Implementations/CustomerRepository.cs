﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using KPZ_Lab_3_DB_First.Data;
using KPZ_Lab_3_DB_First.Models;
using KPZ_Lab_3_DB_First.Repositories.Interfaces;
using Microsoft.EntityFrameworkCore;

namespace KPZ_Lab_3_DB_First.Repositories.Implementations
{
    public class CustomerRepository : BaseRepository<Customer>, ICustomerRepository
    {
        public CustomerRepository (FishingStoreDbContext context) : base(context) { }

        public IEnumerable<Customer> GetCustomerWithAddress()
        {
            return GetAll().AsNoTracking()
                .Include(b => b.CustomerAddresses)
                .Where(b => b.CustomerAddresses.Count > 0);
        }
    }
}
