﻿using KPZ_Lab_3_DB_First.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab_3_DB_First.Repositories.Interfaces
{
    public interface IOrderRepository : IBaseRepository<Order>
    {
        public IEnumerable<Order> GetOrderTracking();

        public IEnumerable<Order> GetOrderDetails();
    }
}
