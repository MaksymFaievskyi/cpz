﻿using KPZ_Lab_3_DB_First.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab_3_DB_First.Repositories.Interfaces
{
    public interface IUnitOfWork
    {
        ICustomerRepository CustomerRepository { get; }
        IOrderRepository OrderRepository { get; }
        IProductRepository ProductRepository { get; }
        IPromotionRepository PromotionRepository { get; }
        IReviewRepository ReviewRepository { get; }
        IBaseRepository<CustomerAddress> CustomerAddressRepository { get; }
        IBaseRepository<OrderDetail> OrderDetailRepository { get; }
        IBaseRepository<OrderTracking> OrderTrackingRepository { get; }
        IBaseRepository<ProductCategory> ProductCategoryRepository { get; }
        IBaseRepository<ShoppingCart> ShoppingCartRepository { get; }
        void Commit();
        Task CommitAsync();
    }
}
