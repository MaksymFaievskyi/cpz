﻿using KPZ_Lab_3_DB_First.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab_3_DB_First.Repositories.Interfaces
{
    public interface IReviewRepository : IBaseRepository <Review> 
    {

    }
}
