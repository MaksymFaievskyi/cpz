﻿using KPZ_Lab_3_DB_First.Models;
using KPZ_Lab_3_DB_First.Repositories.Implementations;
using Microsoft.EntityFrameworkCore;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KPZ_Lab_3_DB_First.Repositories.Interfaces
{
    public interface IProductRepository : IBaseRepository<Product>
    {
        public IEnumerable<ProductWithCategory> GetProductWithCategory();
        public int GetCategoryByName(string name);

        public string GetCategoryNameById(int categoryId);

    }


}
