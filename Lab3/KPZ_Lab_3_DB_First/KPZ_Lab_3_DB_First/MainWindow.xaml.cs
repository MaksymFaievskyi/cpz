﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using KPZ_Lab_3_DB_First.Models;
using KPZ_Lab_3_DB_First.Repositories.Implementations;
using KPZ_Lab_3_DB_First.Repositories.Interfaces;
using KPZ_Lab_3_DB_First.Views;
using static Microsoft.EntityFrameworkCore.DbLoggerCategory.Database;

namespace KPZ_Lab_3_DB_First
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        UnitOfWork unitOfWork;

        public MainWindow()
        {
            unitOfWork = new UnitOfWork(new Data.FishingStoreDbContext());
            InitializeComponent();
            CustomersTable.ItemsSource = unitOfWork.CustomerRepository.GetAll().ToList();
            ProductsTable.ItemsSource = unitOfWork.ProductRepository.GetProductWithCategory().ToList();
            ReviewsTable.ItemsSource = unitOfWork.ReviewRepository.GetAll().ToList();
            OrdersTable.ItemsSource = unitOfWork.OrderRepository.GetAll().ToList();
            PromotionsTable.ItemsSource = unitOfWork.PromotionRepository.GetAll().ToList();
        }
        //Customers table
        private void CustomersTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer != null)
            {
                FirstNameTextBox.Text = selectedCustomer.FirstName;
                LastNameTextBox.Text = selectedCustomer.LastName;
                GenderTextBox.Text = selectedCustomer.Gender;
                AgeTextBox.Text = selectedCustomer.Age.ToString();
                EmailTextBox.Text = selectedCustomer.Email;
                PhoneNumberTextBox.Text = selectedCustomer.Phone;
            }
        }

        private void DisplayAddressesButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer is not null)
            {
                var customerWithAddress = unitOfWork.CustomerRepository.GetCustomerWithAddress()
                    .Where(b => b.CustomerId == selectedCustomer.CustomerId)
                    .SelectMany(x => x.CustomerAddresses.Select(t => new
                    {
                        t.AddressId,
                        t.CustomerId,
                        t.StreetAddress,
                        t.City,
                        t.PostalCode,
                        t.Country
                    }));

                if (customerWithAddress is not null)
                {
                    CustomerAddressesView customerAddressesView = new CustomerAddressesView();
                    customerAddressesView.CustomerAddressesTable.ItemsSource = customerWithAddress;

                    customerAddressesView.Show();
                }
            }
        }

        private void DisplayShopingCartButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer is not null)
            {
                var customerShoppingCart = unitOfWork.CustomerRepository.GetAll()
                .Where(b => b.CustomerId == selectedCustomer.CustomerId)
                .SelectMany(x => x.ShoppingCarts)
                .ToList(); // Завантажити дані в локальний список

                if (customerShoppingCart is not null)
                {
                    ShopingCartView ShopingCartView = new ShopingCartView();
                    ShopingCartView.ShopingCartTable.ItemsSource = customerShoppingCart;
                    ShopingCartView.Show();
                }
            }
        }

        private void AddCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            var newCustomer = new Customer
            {
                FirstName = FirstNameTextBox.Text,
                LastName = LastNameTextBox.Text,
                Gender = GenderTextBox.Text,
                Age = int.Parse(AgeTextBox.Text),
                Email = EmailTextBox.Text,
                Phone = PhoneNumberTextBox.Text
            };
            unitOfWork.CustomerRepository.Insert(newCustomer);
            unitOfWork.Commit();
            CustomersTable.ItemsSource = unitOfWork.CustomerRepository.GetAll().ToList();

        }
        private void EditCustomerButton_Click(object sender, RoutedEventArgs e)
        {

            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer != null)
            {
                selectedCustomer.FirstName = FirstNameTextBox.Text;
                selectedCustomer.LastName = LastNameTextBox.Text;
                selectedCustomer.Gender = GenderTextBox.Text;
                selectedCustomer.Age = int.Parse(AgeTextBox.Text);
                selectedCustomer.Email = EmailTextBox.Text;
                selectedCustomer.Phone = PhoneNumberTextBox.Text;



                unitOfWork.CustomerRepository.Update(selectedCustomer);
                unitOfWork.Commit();
                CustomersTable.ItemsSource = unitOfWork.CustomerRepository.GetAll().ToList();
            }
        }
        private void DeleteCustomerButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedCustomer = CustomersTable.SelectedItem as Customer;
            if (selectedCustomer != null)
            {
                unitOfWork.CustomerRepository.DeleteIfNotNull(selectedCustomer);
                unitOfWork.Commit();
                CustomersTable.ItemsSource = unitOfWork.CustomerRepository.GetAll().ToList();
            }
        }
        //Products table
        private void ProductsTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedProduct = ProductsTable.SelectedItem as ProductWithCategory;
            
            if (selectedProduct != null)
            {
                //unitOfWork.ProductRepository.GetCategoryByName(selectedProduct.CategoryID)
                CaregoryIDTextBox.Text = selectedProduct.CategoryID.ToString();
                ProductNameTextBox.Text = selectedProduct.ProductName;
                DescriptionTextBox.Text = selectedProduct.Description;
                PriceTextBox.Text = selectedProduct.Price.ToString();
                StockQuantityTextBox.Text = selectedProduct.StockQuantity.ToString();

            }
        }

        private void AddProductButton_Click(object sender, RoutedEventArgs e)
        {
            var newProduct = new Product
            {
                CategoryId =int.Parse(CaregoryIDTextBox.Text),
                ProductName = ProductNameTextBox.Text,
                Description = DescriptionTextBox.Text,
                Price = decimal.Parse(PriceTextBox.Text),
                StockQuantity = int.Parse(StockQuantityTextBox.Text),               
            };
            unitOfWork.ProductRepository.Insert(newProduct);
            unitOfWork.Commit();
            ProductsTable.ItemsSource = unitOfWork.ProductRepository.GetProductWithCategory().ToList();
        }
        private void EditProductButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedProduct = ProductsTable.SelectedItem as ProductWithCategory;
            if (selectedProduct != null)
            {
                selectedProduct.CategoryID = int.Parse(CaregoryIDTextBox.Text);
                selectedProduct.ProductName = ProductNameTextBox.Text;
                selectedProduct.Description = DescriptionTextBox.Text;
                selectedProduct.Price = decimal.Parse(PriceTextBox.Text);
                selectedProduct.StockQuantity = int.Parse(StockQuantityTextBox.Text);

                var editProduct = new Product
                {
                    ProductId = selectedProduct.ProductId,
                    CategoryId = selectedProduct.CategoryID,
                    ProductName = selectedProduct.ProductName,
                    Description = selectedProduct.Description,
                    Price = selectedProduct.Price,
                    StockQuantity = selectedProduct.StockQuantity,
                };

                unitOfWork.ProductRepository.Update(editProduct);
                unitOfWork.Commit();
                ProductsTable.ItemsSource = unitOfWork.ProductRepository.GetProductWithCategory().ToList();
            }
        }
        private void DeleteProductButton_Click(object sender, RoutedEventArgs e)
        {
             var selectedProduct = ProductsTable.SelectedItem as ProductWithCategory;
            if (selectedProduct != null)
            {
                var deleteProduct = new Product
                {
                    ProductId = selectedProduct.ProductId,
                    CategoryId = selectedProduct.CategoryID,
                    ProductName = selectedProduct.ProductName,
                    Description = selectedProduct.Description,
                    Price = selectedProduct.Price,
                    StockQuantity = selectedProduct.StockQuantity,
                };

                unitOfWork.ProductRepository.DeleteIfNotNull(deleteProduct);
                unitOfWork.Commit();
                ProductsTable.ItemsSource = unitOfWork.ProductRepository.GetProductWithCategory().ToList();
            }
        }
        //Reviews table
        private void ReviewsTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedReview = ReviewsTable.SelectedItem as Review;
            if (selectedReview != null)
            {
                ReviewProductIdTextBox.Text = selectedReview.ProductId.ToString();
                ReviewCustomerIdTextBox.Text = selectedReview.CustomerId.ToString();
                ReviewRatingTextBox.Text = selectedReview.Rating.ToString();
                ReviewCommentTextBox.Text = selectedReview.Comment;
                ReviewDateTextBox.Text = selectedReview.ReviewDate.ToString();
            }
        }
        private void AddReviewButton_Click(object sender, RoutedEventArgs e)
        {
            var newReview = new Review
            {
                ProductId = int.Parse(ReviewProductIdTextBox.Text),
                CustomerId = int.Parse(ReviewCustomerIdTextBox.Text),
                Rating = int.Parse(ReviewRatingTextBox.Text),
                Comment = ReviewCommentTextBox.Text,
                ReviewDate = DateTime.Parse(ReviewDateTextBox.Text)
            };
            unitOfWork.ReviewRepository.Insert(newReview);
            unitOfWork.Commit();
            ReviewsTable.ItemsSource = unitOfWork.ReviewRepository.GetAll().ToList();
        }
        private void EditReviewButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedReview = ReviewsTable.SelectedItem as Review;
            if (selectedReview != null)
            {
                selectedReview.ProductId = int.Parse(ReviewProductIdTextBox.Text);
                selectedReview.CustomerId = int.Parse(ReviewCustomerIdTextBox.Text);
                selectedReview.Rating = int.Parse(ReviewRatingTextBox.Text);
                selectedReview.Comment = ReviewCommentTextBox.Text;
                selectedReview.ReviewDate = DateTime.Parse(ReviewDateTextBox.Text);


                unitOfWork.ReviewRepository.Update(selectedReview);
                unitOfWork.Commit();
                ReviewsTable.ItemsSource = unitOfWork.ReviewRepository.GetAll().ToList();
            }
        }
        private void DeleteReviewButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedReview = ReviewsTable.SelectedItem as Review;
            if (selectedReview != null)
            {
                unitOfWork.ReviewRepository.DeleteIfNotNull(selectedReview);
                unitOfWork.Commit();
                ReviewsTable.ItemsSource = unitOfWork.ReviewRepository.GetAll().ToList();
            }
        }
        //Orders table

        private void DisplayOrderStatusButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder is not null)
            {
                var customerOrderTracking = unitOfWork.OrderRepository.GetAll()
                .Where(b => b.OrderId == selectedOrder.OrderId)
                .Select(x => x.OrderTracking)
                .ToList(); // Завантажити дані в локальний список

                if (customerOrderTracking is not null)
                {
                    OrderTrackingView OrderTrackingView = new OrderTrackingView();
                    OrderTrackingView.OrderTrackingTable.ItemsSource = customerOrderTracking;
                    OrderTrackingView.Show();
                }
            }
        }

        private void DisplayOrderDetailsButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder is not null)
            {
                var OrderDetails = unitOfWork.OrderRepository.GetAll()
                .Where(b => b.OrderId == selectedOrder.OrderId)
                .SelectMany(x => x.OrderDetails)
                .ToList(); // Завантажити дані в локальний список

                if (OrderDetails is not null)
                {
                    OrderDetailsView OrderDetailsView = new OrderDetailsView();
                    OrderDetailsView.OrderDetailsTable.ItemsSource = OrderDetails;
                    OrderDetailsView.Show();
                }
            }
        }

        private void OrdersTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder != null)
            {
                CustomerIdTextBox.Text = selectedOrder.CustomerId.ToString();
                OrderDateTextBox.Text = selectedOrder.OrderDate.ToString();
                TotalAmountTextBox.Text = selectedOrder.TotalAmount.ToString();

            }
        }

        private void AddOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var newOrder = new Order
            {
                CustomerId = int.Parse(CustomerIdTextBox.Text),
                OrderDate = DateTime.Parse(OrderDateTextBox.Text),
                TotalAmount = int.Parse(TotalAmountTextBox.Text),
            };
            unitOfWork.OrderRepository.Insert(newOrder);
            unitOfWork.Commit();
            OrdersTable.ItemsSource = unitOfWork.OrderRepository.GetAll().ToList();
        }
        private void EditOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder != null)
            {
                selectedOrder.CustomerId = int.Parse(CustomerIdTextBox.Text);
                selectedOrder.OrderDate = DateTime.Parse(OrderDateTextBox.Text);
                selectedOrder.TotalAmount = int.Parse(TotalAmountTextBox.Text);

                unitOfWork.OrderRepository.Update(selectedOrder);
                unitOfWork.Commit();
                OrdersTable.ItemsSource = unitOfWork.OrderRepository.GetAll().ToList();
            }
        }
        private void DeleteOrderButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedOrder = OrdersTable.SelectedItem as Order;
            if (selectedOrder != null)
            {
                unitOfWork.OrderRepository.DeleteIfNotNull(selectedOrder);
                unitOfWork.Commit();
                OrdersTable.ItemsSource = unitOfWork.OrderRepository.GetAll().ToList();
            }
        }
        //Orders table
        private void PromotionsTable_SelectionChanged(object sender, System.Windows.Controls.SelectionChangedEventArgs e)
        {
            var selectedPromotion = PromotionsTable.SelectedItem as Promotion;
            if (selectedPromotion != null)
            {
                ProductIdTextBox.Text = selectedPromotion.ProductId.ToString();
                DiscountTextBox.Text = selectedPromotion.Discount.ToString();
                StartDateTextBox.Text = selectedPromotion.StartDate.ToString();
                EndDateTextBox.Text = selectedPromotion.EndDate.ToString();

            }
        }
        private void AddPromotionButton_Click(object sender, RoutedEventArgs e)
        {
            var newPromotion = new Promotion
            {
                ProductId = int.Parse(ProductIdTextBox.Text),
                Discount = decimal.Parse(DiscountTextBox.Text),
                StartDate = DateTime.Parse(StartDateTextBox.Text),
                EndDate = DateTime.Parse(EndDateTextBox.Text),
            };
            unitOfWork.PromotionRepository.Insert(newPromotion);
            unitOfWork.Commit();
            PromotionsTable.ItemsSource = unitOfWork.PromotionRepository.GetAll().ToList();
        }
        private void EditPromotionButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedPromotion = PromotionsTable.SelectedItem as Promotion;
            if (selectedPromotion != null)
            {
                selectedPromotion.ProductId = int.Parse(ProductIdTextBox.Text);
                selectedPromotion.Discount = int.Parse(DiscountTextBox.Text);
                selectedPromotion.StartDate = DateTime.Parse(StartDateTextBox.Text);
                selectedPromotion.EndDate = DateTime.Parse(EndDateTextBox.Text);

                unitOfWork.PromotionRepository.Update(selectedPromotion);
                unitOfWork.Commit();
                PromotionsTable.ItemsSource = unitOfWork.PromotionRepository.GetAll().ToList();
            }
        }
        private void DeletePromotionButton_Click(object sender, RoutedEventArgs e)
        {
            var selectedPromotion = PromotionsTable.SelectedItem as Promotion;
            if (selectedPromotion != null)
            {
                unitOfWork.PromotionRepository.DeleteIfNotNull(selectedPromotion);
                unitOfWork.Commit();
                PromotionsTable.ItemsSource = unitOfWork.PromotionRepository.GetAll().ToList();
            }
        }
    }
}
